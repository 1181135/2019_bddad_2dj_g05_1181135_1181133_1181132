CREATE OR REPLACE TRIGGER trgCondutoresVeiculosAssociacoes
    BEFORE INSERT OR UPDATE ON veiculos_condutores
    FOR EACH ROW
    
DECLARE
    numSobreposicoes INTEGER;
    datasSobrepostaS EXCEPTION;
    
    CURSOR datas(dataInicio veiculos_condutores.data_inicio%TYPE, dataFim veiculos_condutores.data_fim%TYPE) IS
        SELECT COUNT(data_inicio)
        FROM veiculos_condutores vc
        WHERE (vc.matricula = :NEW.matricula AND vc.nr_idCivil = :NEW.nr_idCivil) AND vc.data_fim IS NOT NULL AND
            ((dataInicio, dataFim) OVERLAPS (vc.data_inicio, vc.data_fim))
        UNION
        SELECT COUNT(data_inicio)
        FROM veiculos_condutores vc
        WHERE (vc.matricula = :NEW.matricula AND vc.nr_idCivil = :NEW.nr_idCivil) AND vc.data_fim IS NULL AND
            ((dataInicio, dataFim) OVERLAPS (vc.data_inicio, SYSDATE));
    
BEGIN
    IF (:NEW.data_fim IS NULL) THEN
        OPEN datas(:NEW.data_inicio, SYSDATE);
    ELSE
        OPEN datas(:NEW.data_inicio, :NEW.data_fim);
    END IF;
LOOP
    FETCH datas INTO numSobreposicoes;
    EXIT WHEN datas%notfound;
    
    IF ( numSobreposicoes != 0) THEN
        RAISE datasSobrepostas;
    END IF;
    
END LOOP;
CLOSE datas;

EXCEPTION
    WHEN datasSobrepostas THEN
        raise_application_error(-20001, 'Existe uma sobreposição de datas.');
END;


ALTER TRIGGER trgCondutoresVeiculosAssociacoes ENABLE;
ALTER TRIGGER trgCondutoresVeiculosAssociacoes DISABLE;

INSERT INTO veiculos_condutores(matricula, nr_idCivil, data_inicio, data_fim) 
VALUES ('45-PD-98',211234636,TO_DATE('30/janeiro/2018','DD/MON/YY'),TO_DATE('10/fevereiro/2018','DD/MON/YY'));
INSERT INTO veiculos_condutores(matricula, nr_idCivil, data_inicio, data_fim) 
VALUES ('12-OB-99',211234636,TO_DATE('20/fevereiro/2017','DD/MON/YY'),  TO_DATE('07/junho/2017','DD/MON/YY'));
INSERT INTO veiculos_condutores(matricula, nr_idCivil, data_inicio, data_fim) 
VALUES ('45-PD-98'   ,211234636,TO_DATE('15/dezembro/2017','DD/MON/YY') ,TO_DATE('02/fevereiro/2018','DD/MON/YY'));
