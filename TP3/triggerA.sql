CREATE OR REPLACE TRIGGER  trgAtribuicaoPedido
BEFORE INSERT OR UPDATE ON  pedidos_viagens
FOR EACH ROW
        DECLARE
            matr veiculos_condutores.matricula%type;
            nrCivil veiculos_condutores.nr_IdCivil%type;
            dataInicio veiculos_condutores.data_inicio%type;
            limiteKm veiculos.limite_km_semanal%type;
            distanciaKm veiculos.total_km%type;
            segunda date;
            domingo date;
            no_data_found exception;

Begin

segunda := TRUNC( :new.data_hora_recolha_passageiro, 'iw');
domingo := TRUNC( :new.data_hora_recolha_passageiro, 'iw') + 7 - 1/86400;


Select matricula, nr_IdCivil, data_inicio into matr, nrCivil, dataInicio
from veiculos_condutores vc
where :new.data_hora_recolha_passageiro between vc.data_inicio and vc.data_fim 
and nvl((select sum(distancia_km) from pedidos_viagens where matricula = vc.matricula and cod_pedido not in(select cod_pedido from cancelamento) and data_hora_recolha_passageiro between segunda and domingo),0) + :new.distancia_km < 
(select limite_km_semanal from veiculos where matricula = vc.matricula);
    
    :new.matricula := matr;
    :new.nr_IdCivil := nrCivil;
    :new.data_inicio := dataInicio;
     
Exception
    
    when no_data_found then
    raise_application_error(-20001, 'N�o foram encontradas associa��es.');
End;