-- ** eliminar tabelas se existentes **
-- CASCADE CONSTRAINTS para eliminar as restri��es de integridade das chaves prim�rias e chaves �nicas
-- PURGE elimina a tabela da base de dados e da "reciclagem"
DROP TABLE Veiculos                 CASCADE CONSTRAINTS PURGE;
DROP TABLE Condutores               CASCADE CONSTRAINTS PURGE;
DROP TABLE Veiculos_Condutores      CASCADE CONSTRAINTS PURGE;
DROP TABLE Recibos                  CASCADE CONSTRAINTS PURGE;
DROP TABLE Viagens                  CASCADE CONSTRAINTS PURGE;
DROP TABLE Custos_Servicos          CASCADE CONSTRAINTS PURGE;
DROP TABLE Servicos                 CASCADE CONSTRAINTS PURGE;
DROP TABLE Itenerarios_Viagens      CASCADE CONSTRAINTS PURGE;
DROP TABLE Pontos_Turisticos        CASCADE CONSTRAINTS PURGE;
DROP TABLE Pedidos_Viagens          CASCADE CONSTRAINTS PURGE;
DROP TABLE Fatura                   CASCADE CONSTRAINTS PURGE;
DROP TABLE Recibo_Viagem            CASCADE CONSTRAINTS PURGE;
DROP TABLE Cliente                  CASCADE CONSTRAINTS PURGE;
DROP TABLE Cancelamento             CASCADE CONSTRAINTS PURGE;


-- ** criar tabelas **
-- ## tabela Veiculos ##
CREATE TABLE Veiculos (
    matricula         CHAR(8)         CONSTRAINT pk_veiculos_matricula          PRIMARY KEY 
                                      CONSTRAINT ck_veiculos_matricula          CHECK (REGEXP_LIKE(matricula, '^\d{2}-[A-Z]{2}-\d{2}$|^\d{2}-\d{2}-[A-Z]{2}$|^[A-Z]{2}-\d{2}-\d{2}$'))
                                      CONSTRAINT nn_veiculos_matricula          NOT NULL,
    marca             VARCHAR(40)     CONSTRAINT nn_veiculos_marca              NOT NULL,
    modelo            VARCHAR(40)     CONSTRAINT nn_veiculos_modelo             NOT NULL,
    nr_chassis        INTEGER         CONSTRAINT nn_veiculos_nr_chassis         NOT NULL
                                      CONSTRAINT uk_veiculos_nr_chassis         UNIQUE,
    data_matricula    DATE            CONSTRAINT nn_veiculos_data_matricula     NOT NULL,
    limite_km_semanal INTEGER         CONSTRAINT ck_veiculos_limite_km_semanal  CHECK(limite_km_semanal>0),
    total_km          INTEGER         CONSTRAINT ck_veiculos_total_km           CHECK(total_km>=0),
                                      
                                      CONSTRAINT ck_veiculos_limite_km_semanal_total_km  CHECK(limite_km_semanal >= total_km)
                                      
);

-- ## tabela Condutores ##
CREATE TABLE Condutores (
    nr_IdCivil                      INTEGER     CONSTRAINT pk_condutores_nr_IdCivil                     PRIMARY KEY
                                                CONSTRAINT ck_condutores_nr_IdCivil                     CHECK(REGEXP_LIKE(nr_IdCivil, '^\d{9}$')),
                                                
    nr_IdCivil_supervisor           INTEGER,
    nome_condutor                   VARCHAR(40) CONSTRAINT nn_condutores_nome_condutor                  NOT NULL,
    data_nascimento_condutor        DATE        CONSTRAINT nn_condutores_data_nascimento_condutor       NOT NULL,
    nr_carta_conducao               CHAR(10)    CONSTRAINT uk_condutores_nr_carta_conducao              UNIQUE
                                                CONSTRAINT nn_condutores_nr_carta_conducao              NOT NULL,       
    data_validade_carta_conducao    DATE        CONSTRAINT nn_condutores_data_validade_carta_conducao   NOT NULL,
    data_emissao_carta_conducao    DATE        CONSTRAINT nn_condutores_data_emissao_carta_conducao     NOT NULL,
    morada                          VARCHAR(40) CONSTRAINT nn_condutores_morada                         NOT NULL,
    NIF_condutor                    INTEGER     CONSTRAINT nn_condutores_NIF_condutor                   NOT NULL
                                                CONSTRAINT uk_condutores_NIF_condutor                   UNIQUE
                                                 
                                               
);

-- ## tabela Veiculos_Condutores ##
CREATE TABLE Veiculos_Condutores (
    matricula       CHAR(8),
    nr_IdCivil      INTEGER,
    data_inicio     DATE            CONSTRAINT nn_veiculos_condutores_data_inicio                       NOT NULL,    
    data_fim        DATE            CONSTRAINT nn_veiculos_condutores_data_fim                          NOT NULL,
    
                                    CONSTRAINT pk_veiculos_condutores_matricula_nr_idCivil_data_inicio  PRIMARY KEY (matricula, nr_idCivil, data_inicio),
                                    CONSTRAINT ck_Veiculos_Condutores_data_inicio_data_fim              CHECK(data_fim > data_inicio)
);

-- ## tabela Recibos ##
CREATE TABLE Recibos(
    nr_recibo                           INTEGER         CONSTRAINT uk_recibos_nr_recibo                                         UNIQUE,
    nr_IdCivil                          INTEGER,
                                                        CONSTRAINT nn_recibos_num_IDcivil                                       CHECK(REGEXP_LIKE(nr_IdCivil, '^\d{9}$')),
    data_emissao                        DATE            CONSTRAINT nn_recibos_data_emissao                                      NOT NULL,
    valor_total                         NUMERIC(6,2)    CONSTRAINT nn_recibos_valor_total                                       NOT NULL
                                                        CONSTRAINT ck_recibos_valor_total                                       CHECK(valor_total>0),
                                                        
                                                        CONSTRAINT pk_recibos_nr_recibo_nr_IdCivil                              PRIMARY KEY(nr_recibo, nr_IdCivil)
);

-- ## tabela Servi�os ##
CREATE TABLE Servicos(
    cod_servico     INTEGER     CONSTRAINT pk_servicos_cod_servico  PRIMARY KEY
                                CONSTRAINT nn_servicos_cod_servico   NOT NULL
                                CONSTRAINT ck_servicos_cod_servico   CHECK(cod_servico > 0),
    descricao       VARCHAR(40) CONSTRAINT nn_servicos_descricao    NOT NULL,
    
                                CONSTRAINT ck_servicos_descricao    CHECK (REGEXP_LIKE(descricao, '^[Casual, Eco, Luxo, Grupo]'))
);

-- ## tabela Custos_Servicos ##
CREATE TABLE Custos_Servicos (
    cod_servico                     INTEGER         CONSTRAINT nn_custos_servicos_cod_servico                               NOT NULL
                                                    CONSTRAINT ck_custos_servicos_cod_servico                               CHECK(cod_servico > 0),
    data_inicio_custo               DATE            CONSTRAINT nn_custos_servicos_data_inicio_custo                         NOT NULL,  
    data_fim_custo                  DATE            CONSTRAINT nn_custos_servicos_fim_custo                                 NOT NULL,
    preco_base                      NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_preco_base                                NOT NULL
                                                    CONSTRAINT ck_custos_servicos_preco_base                                CHECK(preco_base > 0),
    custo_minuto                    NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_custo_minuto                              NOT NULL
                                                    CONSTRAINT ck_custos_servicos_custo_minimo                              CHECK(custo_minuto > 0),
    custo_km                        NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_custo_km                                  NOT NULL
                                                    CONSTRAINT ck_custos_servicos_custo_km                                  CHECK(custo_km > 0),
    taxa_IVA                        NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_taxa_IVA                                  NOT NULL
                                                    CONSTRAINT ck_custos_servicos_taxa_IVA                                  CHECK(taxa_IVA > 0),
    tempo_maximo_espera_minutos     INTEGER         CONSTRAINT nn_custos_servicos_tempo_maximo_espera_minutos               NOT NULL
                                                    CONSTRAINT ck_custos_servicos_tempo_maximo_espera_minutos               CHECK(tempo_maximo_espera_minutos > 0),
    custo_espera_minuto             NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_custo_espera_minuto                       NOT NULL
                                                    CONSTRAINT ck_custos_servicos_custo_espera_minuto                       CHECK(custo_espera_minuto > 0),
    custo_cancelamento_pedido       NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_custo_cancelamento_pedido                 NOT NULL
                                                    CONSTRAINT ck_custos_servicos_custo_cancelamento_pedido                 CHECK(custo_cancelamento_pedido > 0),
    percentagem_cancelamento        NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_percentagem_cancelamento                  NOT NULL
                                                    CONSTRAINT ck_custos_servicos_percentagem_cancelamento                  CHECK(percentagem_cancelamento > 0),
    percentagem_comissao            NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_percentagem_comissao                      NOT NULL
                                                    CONSTRAINT ck_custos_servicos_percentagem_comissao                      CHECK(percentagem_comissao > 0), 
    percentagem_comissao_supervisor NUMERIC(6,2)    CONSTRAINT nn_custos_servicos_percentagem_comissao_supervisor           NOT NULL
                                                    CONSTRAINT ck_custos_servicos_percentagem_comissao_supervisor           CHECK(percentagem_comissao_supervisor > 0),
    
                                                    CONSTRAINT ck_custos_servicos_data_inicio_custo_data_fim_custo  CHECK(data_fim_custo >= data_inicio_custo),
                                                    CONSTRAINT pk_custos_servicos_cod_servico_data_inicio_custo     PRIMARY KEY (cod_servico, data_inicio_custo)
);

-- ## tabela Itenerarios_Viagens ##
CREATE TABLE Itenerarios_Viagens(
    cod_viagem              INTEGER     CONSTRAINT nn_itenerarios_viagens_cod_viagem            NOT NULL
                                        CONSTRAINT ck_itenerarios_viagens_cod_viagem            CHECK(cod_viagem > 0),
    cod_ponto_turistico     INTEGER     CONSTRAINT nn_itenerarios_viagens_cod_ponto_turistico   NOT NULL
                                        CONSTRAINT ck_itenerarios_viagens_cod_ponto_turistico   CHECK(cod_ponto_turistico > 0),
    hora_passagem           INTEGER     CONSTRAINT nn_itenerarios_viagens_hora_passagem         NOT NULL,
    
                                        CONSTRAINT pk_itenerarios_viagens_cod_viagem_cod_ponto_turistico PRIMARY KEY(cod_viagem,cod_ponto_turistico)
);

-- ## tabela Viagens ##
CREATE TABLE Viagens (
    cod_viagem                  INTEGER         CONSTRAINT pk_viagens_cod_viagem                     PRIMARY KEY
                                                CONSTRAINT ck_viagens_cod_viagem                    CHECK (cod_viagem > 0),
    cod_pedido                  INTEGER         CONSTRAINT nn_viagens_cod_pedido                    NOT NULL
                                                CONSTRAINT uk_viagens_cod_pedido                    UNIQUE,
    atraso_passageiro_minutos   INTEGER         CONSTRAINT nn_viagens_atraso_passageiro_minutos     NOT NULL
                                                CONSTRAINT ck_viagens_atraso_passageiro_minutos     CHECK (atraso_passageiro_minutos >= 0),
    duracao_minutos             INTEGER         CONSTRAINT nn_viagens_duracao_minutos               NOT NULL
                                                CONSTRAINT ck_viagens_duracao_minutos               CHECK (duracao_minutos >= 0)
);

-- ## tabela Pontos_Turisticos ##
CREATE TABLE Pontos_Turisticos(
    cod_ponto_turistico     INTEGER     CONSTRAINT pk_pontos_turisticos_cod_ponto_turistico    PRIMARY KEY
                                        CONSTRAINT nn_pontos_turisticos_cod_ponto_turistico    NOT NULL
                                        CONSTRAINT ck_pontos_turisticos_cod_ponto_turistico    CHECK(cod_ponto_turistico > 0),                                    
    nome_ponto_turistico    VARCHAR(40) CONSTRAINT nn_pontos_turisticos_nome_ponto_turistico   NOT NULL,
    tipo_ponto_turistico    VARCHAR(2)  CONSTRAINT ck_pontos_turisticos_tipo_ponto_turistico   CHECK (REGEXP_LIKE (tipo_ponto_turistico, '^[M,MU,PN,MI]', 'i' ))
                                        CONSTRAINT nn_pontos_turisticos_tipo_ponto_turistico   NOT NULL
);

-- ## tabela Pedidos_Viagens ##
CREATE TABLE Pedidos_Viagens(
    cod_pedido                          INTEGER         CONSTRAINT  uk_pedidos_viagens_cod_pedido                               UNIQUE,
    matricula                           CHAR(8),
    nr_IDcivil                          INTEGER,
    data_inicio                         DATE,
    cod_servico                         INTEGER,
    data_hora_pedido                    TIMESTAMP       CONSTRAINT  nn_pedidos_viagens_data_hora_pedido                         NOT NULL,
    nr_IDcivil_cliente                  INTEGER,

    
    data_hora_recolha_passageiro        TIMESTAMP       CONSTRAINT  nn_pedidos_viagens_data_hora_recolha_passageiro             NOT NULL,
    distancia_km                        NUMERIC(5,2)    CONSTRAINT nn_pedidos_viagens_distancia_km                              NOT NULL
                                                        CONSTRAINT ck_pedidos_viagens_distancia_km                              CHECK(distancia_km > 0),
    cancelado                           CHAR(1)         CONSTRAINT  nn_pedidos_viagens_cancelado                                NOT NULL
                                                        CONSTRAINT  ck_pedidos_viagens_cancelado                                CHECK (cancelado LIKE 'S' OR cancelado LIKE 'N'),
    
   
                                                       CONSTRAINT pk_pedidos_viagens_cod_pedido_cod_servico_nr_idcivil_cliente_nr_idcivil_matricula_data_inicio  PRIMARY KEY(cod_pedido,cod_servico,nr_idcivil_cliente,nr_idcivil,matricula,data_inicio),
                                                        CONSTRAINT ck_pedidos_viagens_data_recolha_passageiro_data_hora_pedido  CHECK (data_hora_recolha_passageiro > data_hora_pedido)   
);


-- ## tabela Recibo_Viagem ##
CREATE TABLE Recibo_Viagem(
    cod_viagem                          INTEGER,
    nr_recibo                           INTEGER,
    cod_servico                         INTEGER,
    custo_total                         NUMERIC(6,2)            CONSTRAINT nn_recibo_viagem_custo_total             NOT NULL
                                                                CONSTRAINT ck_recibo_viagem_custo_total             CHECK(custo_total>0),            
    percentagem_comissao                NUMERIC(6,2)            CONSTRAINT nn_recibo_viagem_percentagem_comissao    NOT NULL
                                                                CONSTRAINT ck_recibo_viagem_percentagem_comissao    CHECK(percentagem_comissao>0),
    valor_omissao                       NUMERIC(6,2)            CONSTRAINT nn_recibo_viagem_valor_omissao           NOT NULL
                                                                CONSTRAINT ck_recibo_viagem_valor_omissao           CHECK(valor_omissao>0),
                                                                
                                                                CONSTRAINT pk_recibo_viagem_cod_viagem_nr_recibo_cod_servico      PRIMARY KEY(cod_viagem,nr_recibo,cod_servico)


);

-- ## tabela cliente ##
CREATE TABLE Cliente(
    nr_idCivil_cliente          INTEGER         CONSTRAINT pk_nr_idCivil_cliente                        PRIMARY KEY
                                                CONSTRAINT ck_nr_idCivil_cliente                        CHECK(REGEXP_LIKE(nr_idCivil_cliente, '^\d{9}$')),
    nome_cliente                VARCHAR(40)     CONSTRAINT nn_nome_cliente_cliente                      NOT NULL,
    NIF_cliente                 INTEGER         CONSTRAINT nn_NIF_cliente_cliente                       NOT NULL
                                                CONSTRAINT ck_NIF_cliente_cliente                       CHECK(REGEXP_LIKE(NIF_cliente, '^\d{9}$'))
                                                CONSTRAINT uk_NIF_cliente_cliente                       UNIQUE,
    data_nascimento_cliente     DATE            CONSTRAINT nn_data_nascimento_cliente_cliente           NOT NULL,
    login                       VARCHAR(40)     CONSTRAINT nn_login_cliente                             NOT NULL,
    password_cliente            VARCHAR(40)     CONSTRAINT nn_password_cliente_cliente                  NOT NULL
);


-- ## tabela Fatura ##
CREATE TABLE Fatura (
    cod_fatura              INTEGER,
    cod_viagem              INTEGER,
    custo_total             NUMERIC(6,2)        CONSTRAINT nn_fatura_custo_total            NOT NULL
                                                                                            CHECK(custo_total>0),
                                               
                                                CONSTRAINT pk_fatura_cod_fatura_cod_viagem  PRIMARY KEY(cod_fatura,cod_viagem)
);
-- ## tabela Cancelamento ##
CREATE TABLE Cancelamento(
    cod_pedido                  INTEGER,
    data_hora_cancelamento      DATE            CONSTRAINT nn_cancelamento_data_hora_cancelamento       NOT NULL,
    custo_total_cancelamento    NUMERIC(6,2)    CONSTRAINT ck_cancelamento_custo_total_cancelamento     CHECK(custo_total_cancelamento>0),
                                        CONSTRAINT pk_cancelamento_cod_pedido       PRIMARY KEY(cod_pedido)
);

-- ** alterar tabelas para defini��o de chaves estrangeiras **
-- ## tabela Veiculos_Condutores ##
ALTER TABLE Veiculos_Condutores     ADD CONSTRAINT fk_veiculos_condutores_matricula                     FOREIGN KEY (matricula)                             REFERENCES Veiculos (matricula);
ALTER TABLE Veiculos_Condutores     ADD CONSTRAINT fk_veiculos_condutores_nr_idCivil                    FOREIGN KEY (nr_idCivil)                            REFERENCES Condutores (nr_idCivil);

-- ## tabela Condutores ##
--ALTER TABLE Condutores ADD CONSTRAINT fk_condutores_nr_idCivil_supervisor FOREIGN KEY (nr_idCivil_supervisor) REFERENCES Condutores (nr_idCivil);

-- ## tabela Recibos ##
ALTER TABLE Recibos         ADD CONSTRAINT fk_recibos_nr_idcivil                     FOREIGN KEY (nr_IdCivil)  REFERENCES Condutores(nr_IdCivil);  

-- ## tabela Pedidos_Viagens ##
ALTER TABLE Pedidos_Viagens         ADD CONSTRAINT fk_pedidos_viagens_cod_servico                       FOREIGN KEY (cod_servico)                           REFERENCES Servicos(cod_servico);
ALTER TABLE Pedidos_Viagens         ADD CONSTRAINT fk_pedidos_viagens_matricula_nr_idcivil_data_inicio  FOREIGN KEY (matricula, nr_idcivil, data_inicio)    REFERENCES Veiculos_Condutores (matricula, nr_idcivil, data_inicio);
ALTER TABLE Pedidos_Viagens         ADD CONSTRAINT fk_pedido_viagens_nr_idcivil_cliente                 FOREIGN KEY (nr_idcivil_cliente)                    REFERENCES Cliente(nr_idcivil_cliente);

-- ## tabela Recibo Viagem
ALTER TABLE Recibo_Viagem       ADD CONSTRAINT fk_recibo_viagem_cod_viagem         FOREIGN KEY(cod_viagem) REFERENCES Viagens(cod_viagem);
ALTER TABLE Recibo_Viagem       ADD CONSTRAINT fk_recibo_viagem_nr_recibo         FOREIGN KEY(nr_recibo) REFERENCES Recibos(nr_recibo);
ALTER TABLE Recibo_Viagem       ADD CONSTRAINT fk_recibo_viagem_cod_servico         FOREIGN KEY(cod_servico) REFERENCES Servicos(cod_servico);

--- ## tabela cancelamento
ALTER TABLE Cancelamento        ADD CONSTRAINT fk_cancelamento_cod_pedido  FOREIGN KEY (cod_pedido) REFERENCES Pedidos_Viagens(cod_pedido);

-- ## tabela Fatura ## --
ALTER TABLE Fatura         ADD CONSTRAINT fk_fatura_cod_cod_viagem                       FOREIGN KEY (cod_viagem)                           REFERENCES Viagens(cod_viagem);

-- ## tabela Viagens ##
ALTER TABLE Viagens                 ADD CONSTRAINT fk_viagens_cod_pedido                                FOREIGN KEY (cod_pedido)                            REFERENCES Pedidos_Viagens(cod_pedido);

-- ## tabela Itenerarios_Viagens ##
ALTER TABLE Itenerarios_Viagens     ADD CONSTRAINT fk_itenerarios_viagens_cod_viagem                    FOREIGN KEY (cod_viagem)                            REFERENCES Viagens(cod_viagem);
ALTER TABLE Itenerarios_Viagens     ADD CONSTRAINT fk_itenerarios_viagens_cod_ponto_turistico           FOREIGN KEY (cod_ponto_turistico)                   REFERENCES Pontos_Turisticos(cod_ponto_turistico);



--se necess�rio por causa de problemas com o  REGEXP_LIKE
ALTER SESSION SET NLS_SORT = BINARY;