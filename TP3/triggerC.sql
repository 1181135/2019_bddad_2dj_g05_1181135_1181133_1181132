CREATE OR REPLACE TRIGGER trgCondutoresImpedirSupervisores
        BEFORE INSERT OR UPDATE ON condutores
        FOR EACH ROW
        DECLARE
            idade_supervisor_invalido EXCEPTION;
            emissao_carta_invalido EXCEPTION;
            nr_viagens_invalido EXCEPTION;
            numero_viagens_condutor int;
            numero_viagens_supervisor int;
            data_nascimento_supervisor CONDUTORES.DATA_NASCIMENTO_CONDUTOR%type;
            data_emissao_carta_conducao_supervisor CONDUTORES.DATA_EMISSAO_CARTA_CONDUCAO%type;
        Begin
            IF(:new.nr_IdCivil_supervisor IS NOT NULL) THEN
            
                SELECT c.data_nascimento_condutor,c.data_emissao_carta_conducao into data_nascimento_supervisor, data_emissao_carta_conducao_supervisor
                FROM condutores c
                WHERE c.nr_IdCivil = :new.nr_IdCivil_supervisor;
                
                SELECT count(pv.cod_pedido) into numero_viagens_condutor
                FROM condutores c
                    INNER JOIN veiculos_condutores vc ON c.nr_IdCivil = vc.nr_IdCivil
                    INNER JOIN pedidos_viagens pv ON vc.nr_IdCivil = pv.nr_IdCivil
                    INNER JOIN viagens v ON pv.cod_pedido = v.cod_pedido
                WHERE c.nr_IdCivil = :new.nr_IdCivil;
                
                select count(pv.cod_pedido) into numero_viagens_supervisor
                FROM condutores c
                    INNER JOIN veiculos_condutores vc ON c.nr_IdCivil = vc.nr_IdCivil
                    INNER JOIN pedidos_viagens pv ON vc.nr_IdCivil = pv.nr_IdCivil
                    INNER JOIN viagens v ON pv.cod_pedido = v.cod_pedido
                WHERE c.nr_IdCivil = :new.nr_IdCivil_supervisor;
                --IF((MONTHS_BETWEEN(data_nascimento_supervisor,:new.data_nascimento_condutor)/12) < 5)THEN
                IF((TRUNC(:new.data_nascimento_condutor)-TRUNC(data_nascimento_supervisor)) < 5*365)THEN
                    RAISE idade_supervisor_invalido;
                ELSIF(numero_viagens_supervisor < numero_viagens_condutor)THEN
                    RAISE nr_viagens_invalido;
                ELSIF(:new.data_emissao_carta_conducao < data_emissao_carta_conducao_supervisor)THEN
                    RAISE emissao_carta_invalido;
                END IF;
            END IF;
            EXCEPTION
                WHEN idade_supervisor_invalido THEN
                    raise_application_error(-20001,'As idades entre o condutor e o supervisor n�o diferem em 5 anos');
                WHEN nr_viagens_invalido THEN
                    raise_application_error(-20001,'O n�mero de viagens do condutor s�o superiores ao n�mero de viagens do supervisor');
                WHEN emissao_carta_invalido THEN
                    raise_application_error(-20001,'O condutor t�m a carta de condu��o h� mais tempo que o supervisor');
END;

--Teste para menos de 5 anos
    insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(2112346, 100000012,'Pedro ze', TO_DATE('14/janeiro/1990','DD/MON/YY'), TO_DATE('14/maio/2010','DD/MON/YY'),'P-01234568', TO_DATE('16/dezembro/2023','DD/MON/YY'), 'Rua A', 600000001);


--Teste para mais de 5 anos mas com carta � menos tempo(supervisor)
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(211346, 100000012,'Pedro ze', TO_DATE('14/janeiro/1996','DD/MON/YY'), TO_DATE('14/maio/2009','DD/MON/YY'),'P-01234568', TO_DATE('16/dezembro/2023','DD/MON/YY'), 'Rua A', 600000001);



