CREATE OR REPLACE TRIGGER trgCalculoCustoCancelamento
    BEFORE INSERT OR UPDATE  ON cancelamento 
    FOR EACH ROW 
        DECLARE
            var_data_pedido pedidos_viagens.data_hora_pedido%TYPE;
            var_data_recolha pedidos_viagens.data_hora_recolha_passageiro%TYPE;
            var_custo_inicial custos_servicos.custo_cancelamento_pedido%TYPE;
            var_distancia pedidos_viagens.distancia_km%TYPE;
            var_taxa custos_servicos.percentagem_cancelamento%TYPE;
            var_km_custo custos_servicos.custo_km%TYPE;
            ex_data_invalida EXCEPTION;

        BEGIN 
            SELECT data_hora_pedido into  var_data_pedido
            FROM pedidos_viagens
            WHERE :NEW.cod_pedido = pedidos_viagens.cod_pedido;

            SELECT data_hora_recolha_passageiro into  var_data_recolha
            FROM pedidos_viagens
            WHERE :NEW.cod_pedido = pedidos_viagens.cod_pedido;

            IF(:NEW.data_hora_cancelamento-var_data_pedido  > 0) THEN 
                RAISE ex_data_invalida;
            END IF;

            IF(:NEW.data_hora_cancelamento-var_data_recolha  > 0) THEN 
                RAISE ex_data_invalida;
            END IF;
            
            IF((:NEW.data_hora_cancelamento-var_data_recolha)* 24 >= 1) THEN 
                SELECT custo_cancelamento_pedido into var_custo_inicial
                FROM custos_servicos INNER JOIN 
                servicos ON 
                servicos.cod_servico = custos_servicos.cod_servico
                INNER JOIN 
                pedidos_viagens ON
                servicos.cod_servico = pedidos_viagens.cod_servico
                WHERE pedidos_viagens.cod_pedido = :new.cod_pedido;

            :NEW.custo_total_cancelamento := var_custo_inicial;

            ELSE 
                SELECT custo_cancelamento_pedido into var_custo_inicial
                FROM custos_servicos INNER JOIN 
                servicos ON 
                servicos.cod_servico = custos_servicos.cod_servico
                INNER JOIN 
                pedidos_viagens ON
                servicos.cod_servico = pedidos_viagens.cod_servico
                WHERE pedidos_viagens.cod_pedido =:new.cod_pedido;

                SELECT percentagem_cancelamento into var_taxa
                FROM custos_servicos INNER JOIN 
                servicos ON 
                servicos.cod_servico = custos_servicos.cod_servico
                INNER JOIN 
                pedidos_viagens ON
                servicos.cod_servico = pedidos_viagens.cod_servico
                WHERE  pedidos_viagens.cod_pedido = :new.cod_pedido;

                SELECT custo_km INTO var_km_custo
                FROM custos_servicos INNER JOIN 
                servicos ON 
                servicos.cod_servico = custos_servicos.cod_servico
                INNER JOIN 
                pedidos_viagens ON
                servicos.cod_servico = pedidos_viagens.cod_servico
                WHERE pedidos_viagens.cod_pedido = :NEW.cod_pedido;

                SELECT distancia_km INTO var_distancia
                FROM pedidos_viagens 
                WHERE :NEW.cod_pedido = pedidos_viagens.cod_pedido;
                :NEW.custo_total_cancelamento:=var_custo_inicial + var_custo_inicial*var_distancia*var_taxa*var_km_custo;
            END IF;

        EXCEPTION
            WHEN ex_data_invalida THEN
                raise_application_error(-20002, 'Horario nao suportado.');
            WHEN no_data_found THEN
                raise_application_error(-20001, 'Codigo pedido n�o encontrado.');
END;

/
ALTER TRIGGER trgCalculoCustoCancelamento ENABLE;

/
ALTER TRIGGER trgCalculoCustoCancelamento DISABLE;

/
INSERT INTO pedidos_viagens(cod_pedido ,matricula ,nr_idCivil,data_inicio, cod_servico,  data_hora_pedido, nr_IDcivil_cliente, data_hora_recolha_passageiro, distancia_km, cancelado)
VALUES (151,'45-AA-12', 213478900, TO_DATE('14/janeiro/2018', 'DD/MON/YY'), 8, TO_DATE('2019-12-12 10:10:00', 'yyyy-mm-dd hh24:mi:ss'), 373800044, TO_DATE('2019-12-12 10:20:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 'S');
INSERT INTO pedidos_viagens(cod_pedido ,matricula ,nr_idCivil,data_inicio, cod_servico,  data_hora_pedido, nr_IDcivil_cliente, data_hora_recolha_passageiro, distancia_km, cancelado)
VALUES (122,'45-AB-12', 213478901,TO_DATE('14/janeiro/2019', 'DD/MON/YY'), 7, TO_DATE('2018-12-12 10:10:00', 'yyyy-mm-dd hh24:mi:ss'), 373800043, TO_DATE('2018-12-12 10:20:00', 'yyyy-mm-dd hh24:mi:ss'), 5, 'S');
INSERT INTO pedidos_viagens(cod_pedido ,matricula ,nr_idCivil,data_inicio, cod_servico,  data_hora_pedido, nr_IDcivil_cliente, data_hora_recolha_passageiro, distancia_km, cancelado)
VALUES (133,'45-AC-12', 213478902, TO_DATE('14/maio/2017', 'DD/MON/YY'), 6, TO_DATE('2016-12-12 10:10:00', 'yyyy-mm-dd hh24:mi:ss'), 373800042, TO_DATE('2017-12-12 10:20:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 'S'); 
INSERT INTO pedidos_viagens(cod_pedido ,matricula ,nr_idCivil,data_inicio, cod_servico,  data_hora_pedido, nr_IDcivil_cliente, data_hora_recolha_passageiro, distancia_km, cancelado)
VALUES (144,'45-AD-12', 213478903, TO_DATE('14/abril/2016', 'DD/MON/YY'), 5, TO_DATE('2015-12-12 10:10:00', 'yyyy-mm-dd hh24:mi:ss'), 373800041, TO_DATE('2016-12-12 10:20:00', 'yyyy-mm-dd hh24:mi:ss'), 7, 'S');

/
--custo inserido, cancelado menos de uma hora antes da recolha
INSERT INTO cancelamento(cod_pedido, data_hora_cancelamento,custo_total_cancelamento)
VALUES (103, TO_DATE('2019-12-12 11:00:00', 'yyyy-mm-dd hh24:mi:ss'),2);

/
--custo atualizado, cancelado mais de uma hora antes da recolha
UPDATE  cancelamento
SET data_hora_cancelamento =  TO_DATE('2019-12-12 09:00:00', 'yyyy-mm-dd hh24:mi:ss')
WHERE cod_pedido = 103;

/
--data de hora de cancelamento superior � data de inicio, levanta exception de horario
INSERT INTO cancelamento(cod_pedido, data_hora_cancelamento,custo_total_cancelamento)
VALUES (106, TO_DATE('2020-12-12 10:15:00', 'yyyy-mm-dd hh24:mi:ss'),0);

/
--codigo de pedido nao existente
INSERT INTO cancelamento(cod_pedido,data_hora_cancelamento,custo_total_cancelamento) 
VALUES(666,TO_DATE('2030-05-19 10:00:00', 'yyyy-mm-dd hh24:mi:ss'),0);

/
