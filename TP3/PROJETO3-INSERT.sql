-- ** inserir dados nas tabelas **

--## tabela Condutores##
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(211234636, 100000001,'Pedro Vieira', TO_DATE('14/janeiro/1990','DD/MON/YY'), TO_DATE('14/maio/2012','DD/MON/YY'),'P-01234568', TO_DATE('16/dezembro/2023','DD/MON/YY'), 'Rua A', 600000001);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(100000012, null,'Cesar Rodrigues', TO_DATE('27/maio/1989','DD/MON/YY'), TO_DATE('14/janeiro/2010','DD/MON/YY'), 'C-04323454', TO_DATE('13/janeiro/2024','DD/MON/YY'),'Rua B',600000002);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(201283921, 100000003,'Miguel Almeida', TO_DATE('13/abril/1950','DD/MON/YY'), TO_DATE('14/janeiro/2000','DD/MON/YY'), 'C-12345453', TO_DATE('04/fevereiro/2023','DD/MON/YY'),'Rua C',600000003);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(293094820, 100000004,'Henrique Madanelo', TO_DATE('03/dezembro/1930','DD/MON/YY'),TO_DATE('14/janeiro/2000','DD/MON/YY'), 'C-25324532', TO_DATE('07/maio/2020','DD/MON/YY'),'Rua D',600000004);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(289332902, 100000005,'Pedro Guerreiro', TO_DATE('20/janeiro/1965','DD/MON/YY'),TO_DATE('14/janeiro/2000','DD/MON/YY'), 'C-92345327', TO_DATE('02/abril/2025','DD/MON/YY'),'Rua E',600000005);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(283724202, 100000006,'Joao Almeida', TO_DATE('28/mar�o/1978','DD/MON/YY'),TO_DATE('14/janeiro/2013','DD/MON/YY'), 'C-97647654', TO_DATE('23/maio/2029','DD/MON/YY'),'Rua F',600000006);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(283740212, 100000007,'Juliana Teixeira', TO_DATE('12/abril/1989','DD/MON/YY'),TO_DATE('14/janeiro/2011','DD/MON/YY'), 'C-03457654', TO_DATE('28/agosto/2039','DD/MON/YY'),'Rua G',600000007);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(283741009, 100000008,'Joana Santos', TO_DATE('11/julho/1964','DD/MON/YY'),TO_DATE('14/janeiro/2001','DD/MON/YY'), 'C-44567465', TO_DATE('25/novembro/2030','DD/MON/YY'),'Rua H',600000008);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(292137491, 100000009,'Rita Gouveia', TO_DATE('11/novembro/1986','DD/MON/YY'),TO_DATE('14/janeiro/2004','DD/MON/YY'), 'C-33543266', TO_DATE('28/janeiro/2026','DD/MON/YY'),'Rua I',600000009);
insert into Condutores(nr_IdCivil, nr_IdCivil_supervisor, nome_condutor, data_nascimento_condutor,data_emissao_carta_conducao, nr_carta_conducao, data_validade_carta_conducao, morada, NIF_condutor) 
    values(201234921, 100000010,'Bruno Rebelo', TO_DATE('29/agosto/1985','DD/MON/YY'), TO_DATE('14/janeiro/2009','DD/MON/YY'),'C-51343567', TO_DATE('31/mar�o/2028','DD/MON/YY'),'Rua J',600000010);
   
   -- ## tabela Veiculos ##
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('45-PD-98', 'BMW', 'AAA', 111,  TO_DATE('22/Abril/2011','DD/MON/YY'),90,65);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('12-OB-99', 'FORD', 'BBB', 222,  TO_DATE('10/maio/2000','DD/MON/YY'),90,39);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km)  
    values('32-SD-21', 'MERCEDES', 'CCC', 333,  TO_DATE('07/setembro/2016','DD/MON/YY'),90,46);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('76-EE-11', 'FORD', 'DDD', 444,  TO_DATE('16/dezembro/2007','DD/MON/YY'),90,76);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km)  
    values('89-AS-12', 'TOYOTA', 'EEE', 555,  TO_DATE('15/janeiro/2009','DD/MON/YY'),90,89);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('15-HD-29', 'MERCEDES', 'FFF', 666,  TO_DATE('15/mar�o/2018','DD/MON/YY'),90,85);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('87-CV-45', 'SEAT', 'GGG', 777,  TO_DATE('30/outubro/2010','DD/MON/YY'),90,82);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('11-FG-56', 'RENAULT', 'HHH', 888,  TO_DATE('01/novembro/1999','DD/MON/YY'),90,78);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('32-RB-32', 'TOYOTA', 'III', 999,  TO_DATE('08/janeiro/2016','DD/MON/YY'),90,21);
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula, limite_km_semanal, total_km) 
    values('34-SC-35', 'TOYOTA', 'JJJ', 123,  TO_DATE('16/dezembro/2011','DD/MON/YY'),90,0);
    
   -- ## tabela Veiculos_Condutores ##
insert into veiculos_condutores(matricula, nr_IdCivil, data_inicio, data_fim) 
    values('45-PD-98',211234636, TO_DATE('26/janeiro/2018','DD/MON/YY'), TO_DATE('15/fevereiro/2018','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('12-OB-99',100000012, TO_DATE('13/maio/2017','DD/MON/YY'), TO_DATE('21/junho/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('32-SD-21', 201283921, TO_DATE('12/abril/2019','DD/MON/YY'), TO_DATE('25/maio/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('76-EE-11', 201283921, TO_DATE('23/novembro/2016','DD/MON/YY'), TO_DATE('15/dezembro/2017','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('89-AS-12', 201283921, TO_DATE('31/dezembro/2015','DD/MON/YY'), TO_DATE('12/fevereiro/2016','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('15-HD-29', 283741009, TO_DATE('16/janeiro/2019','DD/MON/YY'), TO_DATE('11/outubro/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('87-CV-45', 283741009, TO_DATE('26/fevereiro/2018','DD/MON/YY'), TO_DATE('17/maio/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('11-FG-56', 283741009, TO_DATE('23/abril/2011','DD/MON/YY'), TO_DATE('06/janeiro/2017','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('32-RB-32', 201234921, TO_DATE('15/julho/2018','DD/MON/YY'), TO_DATE('07/agosto/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('34-SC-35', 201234921, TO_DATE('08/agosto/2014','DD/MON/YY'), TO_DATE('08/novembro/2018','DD/MON/YY'));
    
    --## tabela Servicos ##
 INSERT INTO servicos(cod_servico,descricao) VALUES(01,'Grupo');
 INSERT INTO servicos(cod_servico,descricao) VALUES(02,'Eco');
 INSERT INTO servicos(cod_servico,descricao) VALUES(03,'Casual');
 INSERT INTO servicos(cod_servico,descricao) VALUES(04,'Luxo');
 
     --## tabela Cliente##
 insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000001,'Pedro Vieira', 700000001, TO_DATE('25/janeiro/1994','DD/MON/YY'), 'pedrovieira', '123456789');
    insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000002,'Joao Almeida', 700000002, TO_DATE('25/maio/1996','DD/MON/YY'), 'joaoalmeida', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000003,'Miguel Almeida', 700000003, TO_DATE('25/janeiro/1983','DD/MON/YY'), 'miguelalmeida', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000004,'Cesar Rodrigues', 700000004, TO_DATE('25/janeiro/1956','DD/MON/YY'), 'cesarrodigues', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000005,'Joana Neves', 700000005, TO_DATE('25/janeiro/2000','DD/MON/YY'), 'joananeves', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000006,'Rita Pereira', 700000006, TO_DATE('25/janeiro/2001','DD/MON/YY'), 'ritapereira', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000007,'Cristiano Arnaldo', 700000013, TO_DATE('25/janeiro/2000','DD/MON/YY'), 'cristianoarnaldo', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000008,'Lionel Roto', 700000007, TO_DATE('25/janeiro/1934','DD/MON/YY'), 'lionelroto', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000009,'Frederico Amendoa', 700000008, TO_DATE('25/janeiro/1934','DD/MON/YY'), 'fredericoamendoa', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000010,'Jose Magusto', 700000009, TO_DATE('25/janeiro/1965','DD/MON/YY'), 'josemagusto', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000011,'Mara Fontes', 700000010, TO_DATE('25/janeiro/1978','DD/MON/YY'), 'marafontes', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000012,'Diana Melo', 700000011, TO_DATE('25/janeiro/1983','DD/MON/YY'), 'dianamelo', '123456789');
     insert into Cliente(nr_idCivil_cliente, nome_cliente, NIF_cliente, data_nascimento_cliente,login,password_cliente)
    values(500000013,'Juliana Videira', 700000012, TO_DATE('25/janeiro/1984','DD/MON/YY'), 'julianavideira', '123456789');
    

 
   --## tabela Pedidos_Viagens##
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(101,'45-PD-98', 211234636, TO_DATE('25/janeiro/2018','DD/MON/YY'), 01, TO_TIMESTAMP('2018-05-31 09:05:43', 'yyyy-mm-dd hh24:mi:ss'),500000001,TO_TIMESTAMP('2018-05-31 10:05:43', 'yyyy-mm-dd hh24:mi:ss'), 23.5,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(102,'12-OB-99', 100000012, TO_DATE('13/maio/2017','DD/MON/YY'),03, TO_TIMESTAMP('2017-09-16 13:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000002, TO_TIMESTAMP('2017-09-16 13:30:00', 'yyyy-mm-dd hh24:mi:ss'), 12.8,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(103,'32-SD-21', 201283921, TO_DATE('12/abril/2019','DD/MON/YY'),03, TO_TIMESTAMP('2019-04-30 23:50:00', 'yyyy-mm-dd hh24:mi:ss'),500000003, TO_TIMESTAMP('2019-05-01 00:40:00', 'yyyy-mm-dd hh24:mi:ss') ,23.6,'S' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(104,'76-EE-11', 201283921, TO_DATE('23/novembro/2016','DD/MON/YY'),04,TO_TIMESTAMP('2017-07-03 18:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000004, TO_TIMESTAMP('2017-07-03 20:00:00', 'yyyy-mm-dd hh24:mi:ss'), 45.8,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(105,'89-AS-12', 201283921, TO_DATE('31/dezembro/2015','DD/MON/YY'), 02,TO_TIMESTAMP('2016-10-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'),500000005, TO_TIMESTAMP('2018-10-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 21.2,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(106,'15-HD-29', 283741009, TO_DATE('16/janeiro/2019','DD/MON/YY'),03, TO_TIMESTAMP('2016-01-12 23:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000006, TO_TIMESTAMP('2016-01-13 00:00:00', 'yyyy-mm-dd hh24:mi:ss'), 5.7,'S' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(107,'87-CV-45', 283741009, TO_DATE('26/fevereiro/2018','DD/MON/YY'),03, TO_TIMESTAMP('2019-08-24 17:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000007, TO_TIMESTAMP('2019-08-24 17:14:00', 'yyyy-mm-dd hh24:mi:ss'), 5.9,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(108,'11-FG-56', 283741009, TO_DATE('23/abril/2011','DD/MON/YY'),01, TO_TIMESTAMP('2018-05-01 22:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000008, TO_TIMESTAMP('2018-05-01 22:05:00', 'yyyy-mm-dd hh24:mi:ss'),2.1,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(109,'32-RB-32', 201234921, TO_DATE('15/julho/2018','DD/MON/YY'), 01,TO_TIMESTAMP('2013-09-10 07:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000009, TO_TIMESTAMP('2013-09-10 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 80.6,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(110,'34-SC-35', 201234921, TO_DATE('08/agosto/2014','DD/MON/YY'),04, TO_TIMESTAMP('2018-12-21 21:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000010, TO_TIMESTAMP('2018-12-21 23:00:00', 'yyyy-mm-dd hh24:mi:ss'), 100,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(111,'11-FG-56', 283741009, TO_DATE('23/abril/2011','DD/MON/YY'), 02,TO_TIMESTAMP('2016-06-16 03:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000001, TO_TIMESTAMP('2016-06-16 03:45:00', 'yyyy-mm-dd hh24:mi:ss'), 27.8,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(112,'12-OB-99', 100000012, TO_DATE('13/maio/2017','DD/MON/YY'),01, TO_TIMESTAMP('2018-08-31 20:00:00', 'yyyy-mm-dd hh24:mi:ss'),500000002, TO_TIMESTAMP('2018-08-31 21:00:00', 'yyyy-mm-dd hh24:mi:ss'), 32,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, nr_IDcivil_cliente,  data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(113,'45-PD-98', 211234636, TO_DATE('25/janeiro/2018','DD/MON/YY'), 03,TO_TIMESTAMP('2018-09-12 10:00:00', 'yyyy-mm-dd hh24:mi:ss'), 500000003,TO_TIMESTAMP('2018-09-12 10:31:00', 'yyyy-mm-dd hh24:mi:ss'), 34.8,'N' );
    
   --## tabela Viagens ##
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(1,101,10,60);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(2,102,10,100);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(3,103,20,80);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(4,104,40,120);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(5,105,50,120);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(6,106,10,60);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(7,107,20,240);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(8,108,15,180);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(9,109,30,160);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(10,110,10,60);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(11,111,15,180);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(12,112,30,160);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(13,113,10,60);
 
 -- ## tabela Pontos_Turisticos ##
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(1,'Torre dos Cl�rigos', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(2,'Torre de Belem', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(3,'Palacio Nacional da Pena', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(4,'Mosteiro dos Jer�nimos', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(5,'Castelo de S.Jorge', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(6,'Museu Nacional Arte Antiga', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(7,'Museu Nacional das Coches', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(8,'Museu Nacional do Azulejo', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(9,'Museu Calouste Gulbenkian', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(10,'Museu Arte Arquitetura e Tecnologia', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(11,'Parque Nacional Peneda-Ger�s', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(12,'Parque Natural da Arr�bida', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(13,'Parque Natural de Sintra-Cascais', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(14,'Parque Natural do Alv�o', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(15,'Parque Natural da Serra da Estrela', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(16,'Miradouro da Vit�ria', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(17,'Miradouro da Serra do Pilar', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(18,'Miradouro Santa Catarina', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(19,'Miradouro das Fontainhas', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(20,'Miradouro do Estu�rio do Douro', 'MI');
 
 
--## tabela Itenerarios_Viagens##
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(1,1,13);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(2,6,22);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(3,11,19);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(4,16,07);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(5,5,09);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(6,2,11);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(7,17,12);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(8,8,16);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(9,13,18);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(10,4,23);
    

--## tabela Custos_Servicos##
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(01, TO_DATE('31/maio/2018','DD/MON/YY'), TO_DATE('31/maio/2018','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20,0.20,0.10,0.05);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(03, TO_DATE('16/novembro/2017','DD/MON/YY'), TO_DATE('16/novembro/2017','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20,0.25,0.15,0.10);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(03, TO_DATE('30/abril/2019','DD/MON/YY'), TO_DATE('01/maio/2019','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20,0.25,0.15,0.10);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(04, TO_DATE('03/julho/2017','DD/MON/YY'), TO_DATE('03/julho/2017','DD/MON/YY'), 10.00, 4.50, 3.05, 0.23, 10, 1.35, 8.35,0.40,0.30,0.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(02, TO_DATE('12/janeiro/2016','DD/MON/YY'), TO_DATE('13/janeiro/2016','DD/MON/YY'), 1.50, 0.20, 0.70, 0.23, 5, 0.10, 1.80,0.10,0.15,0.15);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(03, TO_DATE('24/agosto/2019','DD/MON/YY'), TO_DATE('24/agosto/2019','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20,0.25,0.15,0.10);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(03, TO_DATE('01/maio/2019','DD/MON/YY'), TO_DATE('01/maio/2019','DD/MON/YY'),2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20,0.25,0.15,0.10);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(01, TO_DATE('10/setembro/2013','DD/MON/YY'), TO_DATE('10/setembro/2013','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20,0.20,0.10,0.05);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(01, TO_DATE('21/dezembro/2018','DD/MON/YY'), TO_DATE('21/dezembro/2018','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20,0.20,0.10,0.05);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(04, TO_DATE('16/junho/2015','DD/MON/YY'), TO_DATE('16/junho/2015','DD/MON/YY'), 10.00, 4.50, 3.05, 0.23, 10, 1.35, 8.350,0.40,0.30,0.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(02, TO_DATE('20/mar�o/2014','DD/MON/YY'), TO_DATE('20/mar�o/2014','DD/MON/YY'), 1.50, 0.20, 0.70, 0.23, 5, 0.10, 1.80,0.10,0.15,0.15);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(01, TO_DATE('31/agosto/2017','DD/MON/YY'), TO_DATE('31/agosto/2017','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20,0.20,0.10,0.05);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento,percentagem_comissao,percentagem_comissao_supervisor)
    Values(03, TO_DATE('12/setembro/2018','DD/MON/YY'), TO_DATE('12/setembro/2018','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20,0.25,0.15,0.10);


------------------------------------------------------------------
--## tabela Recibos##
INSERT INTO Recibos(nr_recibo,nr_IdCivil,data_emissao,valor_total)
    Values(1001,211234636,TO_DATE('10/novembro/2019','DD/MON/YY'), 110.35);
INSERT INTO Recibos(nr_recibo,nr_IdCivil,data_emissao,valor_total)
    Values(1002,201283921,TO_DATE('10/novembro/2019','DD/MON/YY'),68.88);
INSERT INTO Recibos(nr_recibo,nr_IdCivil,data_emissao,valor_total)
    Values(1003,283741009,TO_DATE('10/novembro/2019','DD/MON/YY'),85.69);
INSERT INTO Recibos(nr_recibo,nr_IdCivil,data_emissao,valor_total)
    Values(1004,201234921,TO_DATE('10/novembro/2019','DD/MON/YY'), 134.17);


--## tabela Recibo_Viagem##
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(1,1001,01, 60.65,0.10, 3.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(2,1001,03, 13.62,0.20, 4.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(3,1002,03, 24.54,0.20,4.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(4,1002,04, 60.43,0.30,10.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(5,1002,02,7.35,0.05, 1.50);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(6,1003,03, 18.24,0.20,4.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(7,1003,03, 13.47,0.20,4.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(8,1003,01, 37.53,0.10,3.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(9,1004,01, 28.74,0.10,3.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(10,1004,04, 105.43,0.30,10.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(11,1003,02, 16.45,0.05,1.50);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(12,1001,01, 23.45,0.10,3.00);
INSERT INTO Recibo_Viagem(cod_viagem, nr_recibo,cod_servico,custo_total,percentagem_comissao,valor_omissao)
    Values(13,1001,03, 12.63,0.20,4.00);
    
    --## tabela Fatura##
INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1101,1,60.65);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1102,2,13.62);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1103,3,24.54);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1104,4,60.43);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1105,5,7.35);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1106,6,18.24);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1107,7,13.47);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1108,8,37.53);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1109,9,28.74);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1110,10,105.43);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1111,11,16.45);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1112,12,23.45);
    INSERT INTO Fatura(cod_fatura, cod_viagem,custo_total)
    Values(1113,13,12.63);
    
--## tabela Cancelamento##
INSERT INTO Cancelamento(cod_pedido, data_hora_cancelamento,custo_total_cancelamento)
    Values(103,TO_TIMESTAMP('2019-05-01 17:00:00', 'yyyy-mm-dd hh24:mi:ss'),3.60);
INSERT INTO Cancelamento(cod_pedido, data_hora_cancelamento,custo_total_cancelamento)
    Values(106,TO_TIMESTAMP('2016-01-13 17:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2.50);
    


