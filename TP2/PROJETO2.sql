--PROJETO2
--FUNCOES
--1
CREATE OR REPLACE FUNCTION funcTopServico (p_tipo_servico servicos.descricao%TYPE,
                                            p_data_inicial pedidos_viagens.data_hora_recolha_passageiro%TYPE,
                                            p_data_final pedidos_viagens.data_hora_recolha_passageiro%TYPE,
                                            p_n INTEGER) 
                                            RETURN SYS_REFCURSOR IS
    c_id_condutores SYS_REFCURSOR;
    tipo_servico_invalido EXCEPTION;
    flag_tipo_servico INTEGER;
    data_invalida EXCEPTION;
    n_invalido EXCEPTION;

BEGIN

    SELECT COUNT(*) INTO flag_tipo_servico FROM servicos WHERE descricao = p_tipo_servico;  
    IF flag_tipo_servico = 0 THEN
        RAISE tipo_servico_invalido;
    ELSIF p_data_inicial <= 0 OR p_data_final <= 0 THEN 
        RAISE data_invalida;
    ELSIF p_n <= 0 THEN
        RAISE n_invalido;
    ELSE
        OPEN c_id_condutores FOR
    
            WITH
                Custo_Viagem AS (
                    SELECT c.nr_idCivil, s.descricao,((cs.preco_base +(cs.custo_minuto * v.duracao_minutos) +(cs.custo_km * pv.distancia_km) +(cs.custo_espera_minuto * v.atraso_passageiro_minutos)) * (cs.taxa_IVA + 1)) AS custo_total
                    FROM pedidos_viagens pv
                    INNER JOIN viagens v ON pv.cod_pedido = v.cod_pedido
                    INNER JOIN condutores c ON pv.nr_idCivil = c.nr_idCivil
                    INNER JOIN servicos s ON pv.cod_servico = s.cod_servico
                    INNER JOIN custos_servicos cs ON s.cod_servico = cs.cod_servico)
            
            SELECT DISTINCT cv1.nr_idCivil
            FROM Custo_Viagem cv1
            INNER JOIN veiculos_condutores vc ON cv1.nr_idCivil = vc.nr_idCivil
            INNER JOIN pedidos_viagens pv ON vc.matricula = pv.matricula
            INNER JOIN servicos s ON pv.cod_servico = s.cod_servico
            INNER JOIN viagens v ON pv.cod_pedido = v.cod_pedido
            WHERE p_tipo_servico = s.descricao AND p_data_inicial <= pv.data_hora_recolha_passageiro AND p_data_final > pv.data_hora_recolha_passageiro AND cv1.custo_total = (SELECT MAX(cv2.custo_total)
                                                                                                    FROM Custo_Viagem cv2
                                                                                                    WHERE cv1.nr_idCivil = cv2.nr_idCivil)
            
            FETCH FIRST p_n ROWS ONLY;
        
            RETURN c_id_condutores;
    END IF;
        
EXCEPTION
    WHEN tipo_servico_invalido THEN
        RETURN NULL;
    WHEN data_invalida THEN
        RETURN NULL;
    WHEN n_invalido THEN
        RETURN NULL;
END funcTopServico;
/

SET SERVEROUT ON;
DECLARE
    c_id_condutores SYS_REFCURSOR;
    var_id_condutor condutores.nr_idCivil%TYPE;
    
BEGIN
    c_id_condutores := funcTopServico('Luxo', '2010-10-10 10:10:10', '2020-10-10 10:10:10', 3);
    
        LOOP
            FETCH
                c_id_condutores
            INTO
                var_id_condutor;
            EXIT 
        WHEN c_id_condutores%NOTFOUND;
            dbms_output.put_line('ID: ' || var_id_condutor);
        END LOOP;
        CLOSE c_id_condutores;
END;
/
--//--//--//--//--//--//--//--//--//--//--//--
--2

create or replace function funcSobreposicoesVeiculosCondutores return boolean as
    contador integer;
    begin 
    
    select count(*) into contador
    from veiculos_condutores vc1
    inner join veiculos_condutores vc2 on vc1.matricula= vc2.matricula
        and (vc2.data_inicio between vc1.data_inicio and vc1.data_fim) 
        and (vc2.data_fim between vc1.data_inicio and vc1.data_fim) 
        and (vc2.nr_idcivil != vc1.nr_idcivil);
    
    if (contador>0) then
        return true;
    else
        return false;
    end if;
end;
/
SET SERVEROUT ON;
BEGIN
    IF (funcSobreposicoesVeiculosCondutores) THEN 
    DBMS_OUTPUT.PUT_LINE('SOBREPOSICOES = ' || 'TRUE');
  ELSE
    DBMS_OUTPUT.PUT_LINE('SOBREPOSICOES = ' || 'FALSE');
  END IF;
END;

--//--//--//--//--//--//--//--//--//--//--//--
/
-- 3 --
CREATE OR REPLACE FUNCTION funcObterInfoSemanalVeiculos(D DATE) RETURN SYS_REFCURSOR AS
    infoVeiculos SYS_REFCURSOR;
    data DATE;
    domingo DATE;
    sabado DATE;
BEGIN
    data := D;
    LOOP
        EXIT WHEN TO_CHAR(data, 'D') = 7;
        data := data + 1;
    END LOOP;
    sabado := data;
    domingo := sabado - 6;
    OPEN infoVeiculos FOR
        SELECT DISTINCT matricula, data_inicio, data_fim,
               (SELECT COUNT(*)
               FROM Pedidos_Viagens
               WHERE Veiculos_Condutores.matricula = matricula AND
                     data_hora_pedido BETWEEN domingo AND sabado AND
                     data_hora_pedido BETWEEN Veiculos_Condutores.data_inicio AND Veiculos_Condutores.data_fim) AS "Numero Viagens",
               NVL((SELECT SUM(distancia_km)
               FROM pedidos_viagens
               WHERE Veiculos_Condutores.matricula = Matricula AND
                     data_hora_pedido BETWEEN domingo AND sabado AND
                     data_hora_pedido BETWEEN Veiculos_Condutores.data_inicio AND Veiculos_Condutores.data_fim),0) AS "Kilometros",
               NVL((SELECT SUM(duracao_minutos)
               FROM Pedidos_Viagens PV, Viagens V
               WHERE Veiculos_Condutores.matricula = PV.matricula AND
                     PV.cod_pedido = V.cod_pedido AND
                     data_hora_pedido BETWEEN domingo AND sabado AND
                     data_hora_pedido BETWEEN Veiculos_Condutores.data_inicio AND Veiculos_Condutores.data_fim),0) AS "Tempo Gasto Em Viagens"
        FROM Veiculos_Condutores
        WHERE data BETWEEN data_inicio AND data_fim;
    RETURN (infoVeiculos);
END;
/
SET SERVEROUT ON;
DECLARE
    data DATE;
    infoVeiculos SYS_REFCURSOR;
    matricula Veiculos.matricula%TYPE;
    data_inicio Veiculos_condutores.data_inicio%TYPE;
    data_fim Veiculos_Condutores.data_fim%TYPE;
    numViagens INTEGER;
    numKM INTEGER;
    duracao INTEGER;
BEGIN
    data := SYSDATE;
   infoVeiculos := FUNCOBTERINFOSEMANALVEICULOS(data);
    LOOP
        FETCH infoVeiculos INTO matricula, data_inicio, data_fim,numViagens, numKM, duracao;
        EXIT WHEN infoVeiculos%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(data || ' - Matricula: ' || Matricula || ' - ' || data_inicio || ' - ' || data_fim || ' - ' ||numViagens || '� - ' || numKM || 'km - ' || duracao);
    end loop;
    data := TO_DATE('16-06-1999', 'DD-MM-YYYY');
   infoVeiculos := FUNCOBTERINFOSEMANALVEICULOS(data);
    LOOP
        FETCH infoVeiculos INTO matricula, data_inicio, data_fim,numViagens, numKM, duracao;
        EXIT WHEN infoVeiculos%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(data || ' - Matricula: ' || matricula || ' - ' || data_inicio || ' - ' || data_fim || ' - ' ||numViagens || '� - ' || numKM || 'km - ' || duracao);
    end loop;
end;
----teste

--//--//--//--//--//--//--//--//--//--//--//--
--4

CREATE OR REPLACE PROCEDURE procAtualizarCustosServico (p_percentagem IN CUSTOS_SERVICOS.PRECO_BASE%TYPE, p_data_atualizacao IN DATE DEFAULT SYSDATE) IS

    l_custo_base CUSTOS_SERVICOS.PRECO_BASE%TYPE;
    l_custo_minuto CUSTOS_SERVICOS.CUSTO_MINUTO%TYPE;
    l_custo_espera_minuto CUSTOS_SERVICOS.CUSTO_ESPERA_MINUTO%TYPE;
    l_custo_cancelamento CUSTOS_SERVICOS.CUSTO_CANCELAMENTO_PEDIDO%TYPE;
    l_custo_km CUSTOS_SERVICOS.CUSTO_KM%TYPE;
    l_taxa_iva CUSTOS_SERVICOS.TAXA_IVA%TYPE;
    l_tempo_maximo_espera_minutos CUSTOS_SERVICOS.TEMPO_MAXIMO_ESPERA_MINUTOS%TYPE;
    l_cod_servico_atualizar CUSTOS_SERVICOS.COD_SERVICO%TYPE;
    l_data_inicio_custo_atualizar CUSTOS_SERVICOS.DATA_INICIO_CUSTO%TYPE;
    l_custo_total NUMERIC(5, 2);

    ex_precos_atualizados_recentemente EXCEPTION;
    ex_data_invalida EXCEPTION;

    CURSOR c_cod_pedido_atualizar IS
        SELECT CS.COD_SERVICO,
                SUM(CASE PV.CANCELADO
                    WHEN '1' THEN CS.CUSTO_CANCELAMENTO_PEDIDO
                    WHEN '0' THEN (((CS.PRECO_BASE + (CS.CUSTO_MINUTO * V.DURACAO_MINUTOS) + (CS.CUSTO_KM * PV.DISTANCIA_KM) +
                                    (CS.CUSTO_ESPERA_MINUTO * V.ATRASO_PASSAGEIRO_MINUTOS)) * (CS.TAXA_IVA + 1)))
                    END) AS CUSTO_TOTAL
            FROM PEDIDOS_VIAGENS PV
            INNER JOIN VEICULOS_CONDUTORES VC ON PV.MATRICULA = VC.MATRICULA and PV.NR_IDCIVIL = VC.NR_IDCIVIL and PV.DATA_INICIO = VC.DATA_INICIO
            INNER JOIN CONDUTORES C ON PV.NR_IDCIVIL = C.NR_IDCIVIL
            INNER JOIN VIAGENS V ON PV.COD_PEDIDO = V.COD_PEDIDO
            INNER JOIN SERVICOS S ON PV.COD_SERVICO = S.COD_SERVICO
            INNER JOIN CUSTOS_SERVICOS CS ON S.COD_SERVICO = CS.COD_SERVICO
            WHERE PV.DATA_HORA_RECOLHA_PASSAGEIRO BETWEEN ADD_MONTHS(SYSDATE, -12) AND SYSDATE
              AND PV.DATA_HORA_PEDIDO BETWEEN CS.DATA_INICIO_CUSTO AND NVL(CS.DATA_FIM_CUSTO, SYSDATE)
            GROUP BY CS.COD_SERVICO
            ORDER BY CUSTO_TOTAL DESC
            FETCH FIRST ROW ONLY;

BEGIN

    IF p_data_atualizacao < SYSDATE THEN
        RAISE ex_data_invalida;
    END IF;

    OPEN c_cod_pedido_atualizar;
    LOOP
        FETCH c_cod_pedido_atualizar INTO l_cod_servico_atualizar, l_custo_total;
        EXIT WHEN c_cod_pedido_atualizar%NOTFOUND;
    END LOOP;
    CLOSE c_cod_pedido_atualizar;

    SELECT MAX(DATA_INICIO_CUSTO)
    INTO l_data_inicio_custo_atualizar
    FROM CUSTOS_SERVICOS
    WHERE COD_SERVICO LIKE l_cod_servico_atualizar;


    IF ADD_MONTHS(p_data_atualizacao, -6) <= l_data_inicio_custo_atualizar THEN
        RAISE ex_precos_atualizados_recentemente;
    END IF;

    SELECT PRECO_BASE, CUSTO_MINUTO, CUSTO_ESPERA_MINUTO, CUSTO_CANCELAMENTO_PEDIDO, TAXA_IVA, CUSTO_KM, TEMPO_MAXIMO_ESPERA_MINUTOS
    INTO l_custo_base, l_custo_minuto, l_custo_espera_minuto, l_custo_cancelamento, l_taxa_iva, l_custo_km, l_tempo_maximo_espera_minutos
    FROM CUSTOS_SERVICOS
    WHERE COD_SERVICO LIKE l_cod_servico_atualizar AND DATA_INICIO_CUSTO LIKE (SELECT MAX(DATA_INICIO_CUSTO)
                                                                                FROM CUSTOS_SERVICOS
                                                                                WHERE COD_SERVICO LIKE l_cod_servico_atualizar);

    UPDATE CUSTOS_SERVICOS
        SET DATA_FIM_CUSTO = p_data_atualizacao
    WHERE COD_SERVICO LIKE l_cod_servico_atualizar AND DATA_INICIO_CUSTO LIKE (SELECT MAX(DATA_INICIO_CUSTO)
                                                                                FROM CUSTOS_SERVICOS
                                                                                WHERE COD_SERVICO LIKE l_cod_servico_atualizar);

    l_custo_base := l_custo_base + (l_custo_base * p_percentagem / 100);
    l_custo_minuto := l_custo_minuto + (l_custo_minuto * p_percentagem / 100);
    l_custo_espera_minuto := l_custo_espera_minuto + (l_custo_espera_minuto * p_percentagem / 100);
    l_custo_cancelamento := l_custo_cancelamento + (l_custo_cancelamento * p_percentagem / 100);



    INSERT INTO CUSTOS_SERVICOS(COD_SERVICO, DATA_INICIO_CUSTO, DATA_FIM_CUSTO, PRECO_BASE, CUSTO_MINUTO, CUSTO_KM,
                                TAXA_IVA, TEMPO_MAXIMO_ESPERA_MINUTOS, CUSTO_ESPERA_MINUTO, CUSTO_CANCELAMENTO_PEDIDO)
                                VALUES (l_cod_servico_atualizar, p_data_atualizacao, NULL, l_custo_base, l_custo_minuto, l_custo_km, l_taxa_iva,
                                        l_tempo_maximo_espera_minutos, l_custo_espera_minuto, l_custo_cancelamento);

    DBMS_OUTPUT.PUT_LINE('Novo custo base: ' || l_custo_base);
    DBMS_OUTPUT.PUT_LINE('Novo custo minuto: ' || l_custo_minuto);
    DBMS_OUTPUT.PUT_LINE('Novo custo espera minuto: ' || l_custo_espera_minuto);
    DBMS_OUTPUT.PUT_LINE('Novo custo cancelamento pedido: ' || l_custo_cancelamento);

EXCEPTION
    WHEN ex_precos_atualizados_recentemente THEN
        RAISE_APPLICATION_ERROR(-20003, 'Os custos deste servi�o foram atualizados h� menos de 6 meses');
    WHEN ex_data_invalida THEN
        RAISE_APPLICATION_ERROR(-20005, 'A data inserida � menor que a data atual');
END;
/
SET SERVEROUTPUT ON;
BEGIN
procAtualizarCustosServico(5);
END;
/

--5 
CREATE OR REPLACE PROCEDURE procDetetarAssociacoes AS

    mat veiculo.matricula%type;
    numLine INTEGER;
    datIni veiculo_condutor.data_inicio%type;
    datFim veiculo_condutor.data_fim%type;
    datIniSec veiculo_condutor.data_inicio%type;
    datFimSec veiculo_condutor.data_fim%type;
    
    CURSOR veiculos IS
        SELECT DISTINCT matricula
        FROM veiculo;

    CURSOR condutores IS
        SELECT DISTINCT nr_idCilvil
        FROM condutor;

BEGIN
    OPEN veiculos;
    LOOP
        FETCH veiculos INTO mat;
        EXIT WHEN veiculos%notfound;

        CURSOR vcMat IS
            SELECT vc.data_inicio, vc.data_fim, ROW_NUMBER()
            OVER (ORDER BY ve.matricula, vc.data_inicio, vc.data_fim)
            FROM veiculo ve, veiculo_condutor vc
     WHERE ve.matricula = vc.matricula, ve.matricula = mat
            ORDER BY ve.matricula, vc.data_inicio, vc.data_fim;

        OPEN vcMat;
        LOOP
            FETCH vcMat INTO datIni, datFim, numLine;
            EXIT WHEN vcMat%notfound;

            CURSOR vcMatFet IS
                SELECT vc.data_inicio, vc.data_fim
                OFFSET numLine ROWS         --obter as numLine todas
                FROM veiculo ve, veiculo_condutor vc
                WHERE ve.matricula = vc.matricula, ve.matricula = mat
                ORDER BY ve.matricula, vc.data_inicio, vc.data_fim;

            OPEN vcMatFet;
            LOOP
                FETCH vcMatFet INTO datIniSec, datFimSec;
                EXIT WHEN vcMatFet%notfound;

            END LOOP;
            CLOSE vcMatFet;
        END LOOP;
        CLOSE vcMat;
    END LOOP;
    CLOSE veiculos;
END;

--6
/
--TABELA RESUMOS_VEICULOS
DROP TABLE ResumosVeiculos               CASCADE CONSTRAINTS PURGE;

CREATE TABLE ResumosVeiculos ( 
    instante TIMESTAMP CONSTRAINT nn_resumos_veiculos_instante NOT NULL,  
    data_inicio DATE CONSTRAINT nn_resumos_veiculos_data_inicio NOT NULL, 
    data_fim DATE CONSTRAINT nn_resumos_veiculos_data_fim NOT NULL,  
    matricula VARCHAR2(8) CONSTRAINT nn_resumos_veiculos_matricula NOT NULL, 
    nr_viagens INTEGER CONSTRAINT ck_resumos_veiculos_nr_viagens CHECK ( nr_viagens > 0 ), 
    soma_km INTEGER CONSTRAINT ck_resumos_veiculos_soma_km CHECK ( soma_km > 0 ), 
    soma_duracao INTEGER CONSTRAINT ck_resumos_veiculos_soma_duracao CHECK ( soma_duracao > 0 ), 
 
CONSTRAINT ck_resumos_veiculos_data_fim CHECK ( data_fim > data_inicio ) , CONSTRAINT pk_resumos_veiculos_data_inicio_data_fim_matricula PRIMARY KEY ( matricula, data_inicio, data_fim ) ); 
/
--PROCEDIMENTO
CREATE OR REPLACE PROCEDURE procGuardarInformacaoSemanal(p_data pedidos_viagens.data_hora_pedido%TYPE) IS  
    var_cursor SYS_REFCURSOR;
    var_matricula pedidos_viagens.matricula%TYPE;
    var_viagens INTEGER;
    var_distancia INTEGER;
    var_duracao_minutos INTEGER;
    var_data_inicial pedidos_viagens.data_hora_pedido %TYPE;
    var_data_final pedidos_viagens.data_hora_pedido %TYPE;
    var_nr_matriculas_com_viagem INTEGER;
    var_nr_matriculas_sem_viagem INTEGER;
    var_total_matriculas INTEGER;
    var_percentagem_com_viagem NUMBER(10,2);
    var_percentagem_sem_viagem NUMBER(10,2);
    
BEGIN
    var_cursor := funcObterInfoSemanalVeiculos(p_data);
    dbms_output.put_line(' |         Instante                     Matricula             Data Inicio                 Data Fim            Viagens              Distancia               Duracao');
    LOOP
        FETCH var_cursor into var_matricula,var_data_inicial,var_data_final,var_viagens,var_distancia,var_duracao_minutos;
        EXIT WHEN var_cursor%notfound;
        IF(var_viagens = 0) THEN 
            var_viagens := NULL;
        END IF;
        INSERT INTO ResumosVeiculos
        VALUES (p_data,var_data_inicial,var_data_final,var_matricula,var_viagens,var_distancia,var_duracao_minutos);  
        dbms_output.put_line(p_data || '     ' ||var_matricula || '      '|| var_data_inicial ||'     ' || var_data_final || '       ' || var_viagens || '                   ' || var_distancia || '                      ' || var_duracao_minutos);
    END LOOP;
    
    SELECT COUNT(matricula) INTO var_nr_matriculas_com_viagem
    FROM ResumosVeiculos
    WHERE nr_viagens > 0;
    
    SELECT COUNT(matricula) INTO var_nr_matriculas_sem_viagem
    from ResumosVeiculos
    WHERE nr_viagens IS NULL;
    
    SELECT COUNT(matricula) INTO var_total_matriculas
    FROM ResumosVeiculos;
    
    var_percentagem_com_viagem := (var_nr_matriculas_com_viagem / var_total_matriculas) * 100; 
    var_percentagem_sem_viagem := (var_nr_matriculas_sem_viagem / var_total_matriculas) * 100;
    
    dbms_output.put_line('VEICULOS QUE FIZERAM VIAGENS NESSA SEMANA (' || var_percentagem_com_viagem ||'%)'); 
    
    FOR var_matricula IN (SELECT matricula
                        FROM ResumosVeiculos
                        WHERE nr_viagens > 0)
    LOOP
        dbms_output.put_line('                 ' || var_matricula.matricula);  
    END LOOP;
    dbms_output.put_line('VEICULOS QUE NA1O FIZERAM VIAGENS NESSA SEMANA (' || var_percentagem_sem_viagem ||'%)');
    
    FOR var_matricula IN (SELECT matricula
                        FROM ResumosVeiculos
                        WHERE nr_viagens IS NULL)
    LOOP
        dbms_output.put_line('                  ' || var_matricula.matricula);  
    END LOOP;

END;
/
--TESTES
SET SERVEROUTPUT ON;

DELETE FROM ResumosVeiculos;

BEGIN
procGuardarInformacaoSemanal(TO_DATE('2019-10-12', 'yyyy-mm-dd'));
END;
/


