-- ** eliminar tabelas se existentes **
-- CASCADE CONSTRAINTS para eliminar as restri��es de integridade das chaves prim�rias e chaves �nicas
-- PURGE elimina a tabela da base de dados e da "reciclagem"
DROP TABLE Veiculos               CASCADE CONSTRAINTS PURGE;
DROP TABLE Veiculos_Condutores    CASCADE CONSTRAINTS PURGE;
DROP TABLE Condutores             CASCADE CONSTRAINTS PURGE;
DROP TABLE Viagens                CASCADE CONSTRAINTS PURGE;
DROP TABLE Custos_Servicos        CASCADE CONSTRAINTS PURGE;
DROP TABLE Servicos               CASCADE CONSTRAINTS PURGE;
DROP TABLE Itenerarios_Viagens    CASCADE CONSTRAINTS PURGE;
DROP TABLE Pontos_Turisticos      CASCADE CONSTRAINTS PURGE;
DROP TABLE Pedidos_Viagens        CASCADE CONSTRAINTS PURGE;


-- ** criar tabelas **
-- ## tabela Veiculos ##
CREATE TABLE Veiculos (
    matricula       CHAR(8)         CONSTRAINT pk_veiculos_matricula        PRIMARY KEY 
                                    CONSTRAINT ck_veiculos_matricula        CHECK (REGEXP_LIKE(matricula, '^\d{2}-[A-Z]{2}-\d{2}$|^\d{2}-\d{2}-[A-Z]{2}$|^[A-Z]{2}-\d{2}-\d{2}$'))
                                    CONSTRAINT nn_veiculos_matricula        NOT NULL,
    marca           VARCHAR(40)     CONSTRAINT nn_veiculos_marca            NOT NULL,
    modelo          VARCHAR(40)     CONSTRAINT nn_veiculos_modelo           NOT NULL,
    nr_chassis      INTEGER         CONSTRAINT nn_veiculos_nr_chassis       NOT NULL
                                    CONSTRAINT uk_veiculos_nr_chassis       UNIQUE,
    data_matricula  DATE            CONSTRAINT nn_veiculos_data_matricula   NOT NULL
);

-- ## tabela Veiculos_Condutores ##
CREATE TABLE Veiculos_Condutores (
    matricula       CHAR(8),
    nr_idcivil      INTEGER,
    data_inicio     DATE            CONSTRAINT nn_veiculos_condutores_data_inicio                       NOT NULL,    
    data_fim        DATE            CONSTRAINT nn_veiculos_condutores_data_fim                          NOT NULL,
    
                                    CONSTRAINT pk_veiculos_condutores_matricula_nr_idCivil_data_inicio  PRIMARY KEY (matricula, nr_idCivil, data_inicio),
                                    CONSTRAINT ck_Veiculos_Condutores_data_inicio_data_fim  CHECK(data_fim > data_inicio)
);

-- ## tabela Condutores ##
CREATE TABLE Condutores (
    nr_idcivil                      INTEGER     CONSTRAINT pk_condutores_nr_idcivil                     PRIMARY KEY
                                                CONSTRAINT ck_condutores_nr_idcivil                     CHECK(REGEXP_LIKE(nr_idcivil, '^\d{9}$')),
    nr_idcivil_supervisor           INTEGER,   
    nome                            VARCHAR(40) CONSTRAINT nn_condutores_nome                           NOT NULL,
    data_nascimento                 DATE        CONSTRAINT nn_condutores_data_nascimento                NOT NULL,
    nr_carta_conducao               CHAR(10)    CONSTRAINT ck_condutores_nr_carta_conducao              CHECK(REGEXP_LIKE(nr_carta_conducao, '^[A-Z]{1}-\d{8}$'))
                                                CONSTRAINT uk_condutores_nr_carta_conducao              UNIQUE
                                                CONSTRAINT nn_condutores_nr_carta_conducao              NOT NULL,
    data_validade_carta_conducao    DATE        CONSTRAINT nn_condutores_data_validade_carta_conducao   NOT NULL
);

-- ## tabela Viagens ##
CREATE TABLE Viagens (
   cod_viagem                  INTEGER         CONSTRAINT pk_viagens_cod_viagem                    PRIMARY KEY
                                                CONSTRAINT ck_viagens_cod_viagem                    CHECK (cod_viagem > 0),
    cod_pedido                  INTEGER,
    atraso_passageiro_minutos   INTEGER         CONSTRAINT nn_viagens_atraso_passageiro_minutos    NOT NULL
                                                CONSTRAINT ck_viagens_atraso_passageiro_minutos    CHECK (atraso_passageiro_minutos >= 0),
    duracao_minutos             INTEGER         CONSTRAINT nn_viagens_duracao_minutos               NOT NULL
                                                CONSTRAINT ck_viagens_duracao_minutos               CHECK (duracao_minutos >= 0)
);

-- ## tabela Custos_Servicos ##
CREATE TABLE Custos_Servicos (
    cod_servico                     INTEGER         CONSTRAINT nn_viagens_cod_servico                               NOT NULL
                                                    CONSTRAINT ck_viagens_cod_servico                               CHECK(cod_servico > 0),
    data_inicio_custo               DATE            CONSTRAINT nn_viagens_data_inicio_custo                         NOT NULL,  
    data_fim_custo                  DATE            CONSTRAINT nn_viagens_data_fim_custo                            NOT NULL,
    preco_base                      NUMERIC(5,2)    CONSTRAINT nn_viagens_preco_base                                NOT NULL
                                                    CONSTRAINT ck_viagens_preco_base                                CHECK(preco_base > 0),
    custo_minuto                    NUMERIC(5,2)    CONSTRAINT nn_viagens_custo_minuto                              NOT NULL
                                                    CONSTRAINT ck_viagens_custo_minimo                              CHECK(custo_minuto > 0),
    custo_km                        NUMERIC(5,2)    CONSTRAINT nn_viagens_custo_km                                  NOT NULL
                                                    CONSTRAINT ck_viagens_custo_km                                  CHECK(custo_km > 0),
    taxa_IVA                        NUMERIC(5,2)    CONSTRAINT nn_viagens_taxa_IVA                                  NOT NULL
                                                    CONSTRAINT ck_viagens_taxa_IVA                                  CHECK(taxa_IVA > 0),
    tempo_maximo_espera_minutos     INTEGER         CONSTRAINT nn_viagens_tempo_maximo_espera_minutos               NOT NULL
                                                    CONSTRAINT ck_viagens_tempo_maximo_espera_minutos               CHECK(tempo_maximo_espera_minutos > 0),
    custo_espera_minuto             NUMERIC(5,2)    CONSTRAINT nn_viagens_custo_espera_minuto                       NOT NULL
                                                    CONSTRAINT ck_viagens_custo_espera_minuto                       CHECK(custo_espera_minuto > 0),
    custo_cancelamento_pedido       NUMERIC(5,2)    CONSTRAINT nn_viagens_custo_cancelamento_pedido                 NOT NULL
                                                    CONSTRAINT ck_viagens_custo_cancelamento_pedido                 CHECK(custo_cancelamento_pedido > 0),
                                                    
                                                    CONSTRAINT ck_Custos_Servicos_data_inicio_custo_data_fim_custo  CHECK(data_fim_custo >= data_inicio_custo),
                                                    CONSTRAINT pk_custos_servicos_cod_servico_data_inicio_custo     PRIMARY KEY (cod_servico, data_inicio_custo)
);

-- ## tabela Servi�os ##
CREATE TABLE Servicos(
    cod_servico     INTEGER     CONSTRAINT pk_servicos_cod_servico  PRIMARY KEY
                                CONSTRAINT nn_servicos_cod_servico   NOT NULL
                                CONSTRAINT ck_servicos_cod_servico   CHECK(cod_servico > 0),
    descricao       VARCHAR(40) CONSTRAINT nn_servicos_descricao    NOT NULL
                                CONSTRAINT ck_servicos_descricao    CHECK (REGEXP_LIKE(descricao, '^[Casual, Eco, Luxo, Grupo]'))
);

-- ## tabela Itenerarios_Viagens ##
CREATE TABLE Itenerarios_Viagens(
    cod_viagem              INTEGER     CONSTRAINT nn_itenerarios_viagens_cod_viagem            NOT NULL
                                        CONSTRAINT ck_itenerarios_viagens_cod_viagem            CHECK(cod_viagem > 0),
    cod_ponto_turistico     INTEGER     CONSTRAINT nn_itenerarios_viagens_cod_ponto_turistico   NOT NULL
                                        CONSTRAINT ck_itenerarios_viagens_cod_ponto_turistico   CHECK(cod_ponto_turistico > 0),
    hora_passagem           Integer     CONSTRAINT nn_itenerarios_viagens_hora_passagem         NOT NULL,
    
                                        CONSTRAINT pk_itenerarios_viagens_cod_viagem_cod_ponto_turistico PRIMARY KEY(cod_viagem,cod_ponto_turistico)
);

-- ## tabela Pontos_Turisticos ##
CREATE TABLE Pontos_Turisticos(
    cod_ponto_turistico     INTEGER     CONSTRAINT pk_pontos_turisticos_cod_ponto_turistico    PRIMARY KEY
                                        CONSTRAINT nn_pontos_turisticos_cod_ponto_turistico    NOT NULL
                                        CONSTRAINT ck_pontos_turisticos_cod_ponto_turistico    CHECK(cod_ponto_turistico > 0),                                    
    nome_ponto_turistico    VARCHAR(40) CONSTRAINT nn_pontos_turisticos_nome_ponto_turistico   NOT NULL,
    tipo_ponto_turistico    VARCHAR(2)  CONSTRAINT ck_pontos_turisticos_tipo_ponto_turistico   CHECK (REGEXP_LIKE (tipo_ponto_turistico, '^[M,MU,PN,MI]', 'i' ))
                                        CONSTRAINT nn_pontos_turisticos_tipo_ponto_turistico   NOT NULL
);

-- ## tabela Pedidos_Viagens ##
CREATE TABLE Pedidos_Viagens(
    cod_pedido                          INTEGER         CONSTRAINT  pk_pedidos_viagens_cod_pedido                               PRIMARY KEY,
    matricula                           CHAR(8)         CONSTRAINT ck_pedidos_viagens_matricula                                 CHECK (REGEXP_LIKE(matricula, '^\d{2}-[A-Z]{2}-\d{2}$|^\d{2}-\d{2}-[A-Z]{2}$|^[A-Z]{2}-\d{2}-\d{2}$'))
                                                        CONSTRAINT nn_pedidos_viagens_matricula                                 NOT NULL,
    nr_idcivil                          INTEGER         CONSTRAINT nn_pedidos_viagens_nr_idcivil                                NOT NULL,
    data_inicio                         DATE            CONSTRAINT  nn_pedidos_viagens_data_inicio                              NOT NULL,
    cod_servico                         INTEGER         CONSTRAINT nn_pedidos_viagens_cod_servico                               NOT NULL
                                                        CONSTRAINT ck_pedidos_viagens_cod_servico                               CHECK(cod_servico > 0),
    data_hora_pedido                    TIMESTAMP       CONSTRAINT  nn_pedidos_viagens_data_hora_pedido                         NOT NULL,
    data_hora_recolha_passageiro        TIMESTAMP       CONSTRAINT  nn_pedidos_viagens_data_hora_recolha_passageiro             NOT NULL,
    distancia_km                        NUMERIC(5,2)    CONSTRAINT nn_pedidos_viagens_distancia_km                              NOT NULL
                                                        CONSTRAINT ck_pedidos_viagens_distancia_km                              CHECK(distancia_km > 0),
    cancelado                           CHAR(1)         CONSTRAINT  nn_pedidos_viagens_cancelado                                NOT NULL
                                                        CONSTRAINT  ck_pedidos_viagens_cancelado                                CHECK (cancelado LIKE 'S' OR cancelado LIKE 'N'),
                                                        
                                                        CONSTRAINT ck_pedidos_viagens_data_recolha_passageiro_data_hora_pedido  CHECK (data_hora_recolha_passageiro > data_hora_pedido)

    
);


-- ** alterar tabelas para defini��o de chaves estrangeiras **
-- ## tabela Veiculos_Condutores ##
ALTER TABLE Veiculos_Condutores     ADD CONSTRAINT fk_veiculos_condutores_matricula                     FOREIGN KEY (matricula)                             REFERENCES Veiculos (matricula);
ALTER TABLE Veiculos_Condutores     ADD CONSTRAINT fk_veiculos_condutores_nr_idCivil                    FOREIGN KEY (nr_idCivil)                            REFERENCES Condutores (nr_idCivil);

-- ## tabela Condutores ##
--ALTER TABLE Condutores              ADD CONSTRAINT fk_condutores_nr_idCivil_supervisor                  FOREIGN KEY (nr_idCivil_supervisor)                 REFERENCES Condutores(nr_idCivil);

-- ## tabela Viagens ##
ALTER TABLE Viagens                 ADD CONSTRAINT fk_viagens_cod_pedido                                FOREIGN KEY (cod_pedido)                            REFERENCES Pedidos_Viagens(cod_pedido);

-- ## tabela Viagens ##
ALTER TABLE Custos_Servicos         ADD CONSTRAINT fk_custos_servicos_cod_servico                       FOREIGN KEY (cod_servico)                           REFERENCES Servicos(cod_servico);

-- ## tabela Itenerarios_Viagens ##
ALTER TABLE Itenerarios_Viagens     ADD CONSTRAINT fk_itenerarios_viagens_cod_viagem                    FOREIGN KEY (cod_viagem)                            REFERENCES Viagens(cod_viagem);
ALTER TABLE Itenerarios_Viagens     ADD CONSTRAINT fk_itenerarios_viagens_cod_ponto_turistico           FOREIGN KEY (cod_ponto_turistico)                   REFERENCES Pontos_Turisticos(cod_ponto_turistico);

-- ## tabela Pedidos_Viagens ##
ALTER TABLE Pedidos_Viagens         ADD CONSTRAINT fk_pedidos_viagens_cod_servico                       FOREIGN KEY (cod_servico)                           REFERENCES Servicos(cod_servico);
ALTER TABLE Pedidos_Viagens         ADD CONSTRAINT fk_pedidos_viagens_matricula_nr_idcivil_data_inicio  FOREIGN KEY (matricula, nr_idcivil, data_inicio)    REFERENCES Veiculos_Condutores (matricula, nr_idcivil, data_inicio);


--se necess�rio por causa de problemas com o  REGEXP_LIKE
ALTER SESSION SET NLS_SORT = BINARY;