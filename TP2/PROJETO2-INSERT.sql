-- ** inserir dados nas tabelas **

--## tabela Condutores##
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(211234636, 182930419,'Pedro Vieira', TO_DATE('14/janeiro/1990','DD/MON/YY'), 'P-01234568', TO_DATE('16/dezembro/2023','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(218728329, 118341982,'Cesar Rodrigues', TO_DATE('27/maio/1989','DD/MON/YY'), 'C-04323454', TO_DATE('13/janeiro/2024','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(201283921, 182301231,'Miguel Almeida', TO_DATE('13/abril/1950','DD/MON/YY'), 'C-12345453', TO_DATE('04/fevereiro/2023','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(293094820, 128391012,'Henrique Madanelo', TO_DATE('03/dezembro/1930','DD/MON/YY'), 'C-25324532', TO_DATE('07/maio/2020','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(289332902, 175920192,'Pedro Guerreiro', TO_DATE('20/janeiro/1965','DD/MON/YY'), 'C-92345327', TO_DATE('02/abril/2025','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(283724202, 284109283,'Joao Almeida', TO_DATE('28/mar�o/1978','DD/MON/YY'), 'C-97647654', TO_DATE('23/maio/2029','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(283740212, 112837192,'Juliana Teixeira', TO_DATE('12/abril/1989','DD/MON/YY'), 'C-03457654', TO_DATE('28/agosto/2039','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(283741009, 128398270,'Joana Santos', TO_DATE('11/julho/1964','DD/MON/YY'), 'C-44567465', TO_DATE('25/novembro/2030','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(292137491, 198209281,'Rita Gouveia', TO_DATE('11/novembro/1986','DD/MON/YY'), 'C-33543266', TO_DATE('28/janeiro/2026','DD/MON/YY'));
insert into Condutores(nr_idcivil, nr_idcivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao) 
    values(201234921, 129831023,'Bruno Rebelo', TO_DATE('29/agosto/1985','DD/MON/YY'), 'C-51343567', TO_DATE('31/mar�o/2028','DD/MON/YY'));
   
   -- ## tabela Veiculos ##
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('45-PD-98', 'BMW', 'AAA', 111,  TO_DATE('22/Abril/2011','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('12-OB-99', 'FORD', 'BBB', 222,  TO_DATE('10/maio/2000','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('32-SD-21', 'MERCEDES', 'CCC', 333,  TO_DATE('07/setembro/2016','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('76-EE-11', 'FORD', 'DDD', 444,  TO_DATE('16/dezembro/2007','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('89-AS-12', 'TOYOTA', 'EEE', 555,  TO_DATE('15/janeiro/2009','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('15-HD-29', 'MERCEDES', 'FFF', 666,  TO_DATE('15/mar�o/2018','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('87-CV-45', 'SEAT', 'GGG', 777,  TO_DATE('30/outubro/2010','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('11-FG-56', 'RENAULT', 'HHH', 888,  TO_DATE('01/novembro/1999','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('32-RB-32', 'TOYOTA', 'III', 999,  TO_DATE('08/janeiro/2016','DD/MON/YY'));
insert into veiculos(matricula, marca, modelo, nr_chassis, data_matricula) 
    values('34-SC-35', 'TOYOTA', 'JJJ', 123,  TO_DATE('16/dezembro/2011','DD/MON/YY'));
    
   -- ## tabela Veiculos_Condutores ##
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('45-PD-98',211234636, TO_DATE('25/janeiro/2018','DD/MON/YY'), TO_DATE('16/dezembro/2018','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('12-OB-99', 211234636, TO_DATE('21/maio/2017','DD/MON/YY'), TO_DATE('21/junho/2018','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('32-SD-21', 201283921, TO_DATE('12/abril/2019','DD/MON/YY'), TO_DATE('25/maio/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('76-EE-11', 201283921, TO_DATE('23/novembro/2016','DD/MON/YY'), TO_DATE('15/dezembro/2017','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('89-AS-12', 201283921, TO_DATE('31/dezembro/2015','DD/MON/YY'), TO_DATE('12/fevereiro/2016','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('15-HD-29', 283741009, TO_DATE('16/janeiro/2019','DD/MON/YY'), TO_DATE('11/outubro/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('87-CV-45', 283741009, TO_DATE('26/fevereiro/2018','DD/MON/YY'), TO_DATE('17/maio/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('11-FG-56', 283741009, TO_DATE('23/abril/2011','DD/MON/YY'), TO_DATE('06/janeiro/2017','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('32-RB-32', 201234921, TO_DATE('15/julho/2018','DD/MON/YY'), TO_DATE('07/agosto/2019','DD/MON/YY'));
insert into veiculos_condutores(matricula, nr_idcivil, data_inicio, data_fim) 
    values('34-SC-35', 201234921, TO_DATE('08/agosto/2014','DD/MON/YY'), TO_DATE('08/novembro/2018','DD/MON/YY'));
    
    --## tabela Servicos ##
 INSERT INTO servicos(cod_servico,descricao) VALUES(01,'Grupo');
 INSERT INTO servicos(cod_servico,descricao) VALUES(02,'Eco');
 INSERT INTO servicos(cod_servico,descricao) VALUES(03,'Casual');
 INSERT INTO servicos(cod_servico,descricao) VALUES(04,'Luxo');
 
 
   --## tabela Pedidos_Viagens##
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(1,'45-PD-98', 211234636, TO_DATE('25/janeiro/2018','DD/MON/YY'), 01, TO_TIMESTAMP('2018-05-31 09:05:43', 'yyyy-mm-dd hh24:mi:ss'),TO_TIMESTAMP('2018-05-31 10:05:43', 'yyyy-mm-dd hh24:mi:ss'), 23.5,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(2,'12-OB-99', 211234636, TO_DATE('21/maio/2017','DD/MON/YY'), 03, TO_TIMESTAMP('2017-09-16 13:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2017-09-16 13:30:00', 'yyyy-mm-dd hh24:mi:ss'), 12.8,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(3,'32-SD-21', 201283921, TO_DATE('12/abril/2019','DD/MON/YY'), 03, TO_TIMESTAMP('2019-04-30 23:50:00', 'yyyy-mm-dd hh24:mi:ss'),TO_TIMESTAMP('2019-05-01 00:40:00', 'yyyy-mm-dd hh24:mi:ss') ,23.6,'S' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(4,'76-EE-11', 201283921, TO_DATE('23/novembro/2016','DD/MON/YY'), 04,TO_TIMESTAMP('2017-07-03 18:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2017-07-03 20:00:00', 'yyyy-mm-dd hh24:mi:ss'), 45.8,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(5,'89-AS-12', 201283921, TO_DATE('31/dezembro/2015','DD/MON/YY'), 02, TO_TIMESTAMP('2016-10-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2018-10-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 21.2,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(6,'15-HD-29', 283741009, TO_DATE('16/janeiro/2019','DD/MON/YY'), 03, TO_TIMESTAMP('2016-01-12 23:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2016-01-13 00:00:00', 'yyyy-mm-dd hh24:mi:ss'), 5.7,'S' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(7,'87-CV-45', 283741009, TO_DATE('26/fevereiro/2018','DD/MON/YY'), 03, TO_TIMESTAMP('2019-08-24 17:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2019-08-24 17:14:00', 'yyyy-mm-dd hh24:mi:ss'), 5.9,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(8,'11-FG-56', 283741009, TO_DATE('23/abril/2011','DD/MON/YY'), 01, TO_TIMESTAMP('2018-05-01 22:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2018-05-01 22:05:00', 'yyyy-mm-dd hh24:mi:ss'),2.1,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(9,'32-RB-32', 201234921, TO_DATE('15/julho/2018','DD/MON/YY'), 01, TO_TIMESTAMP('2013-09-10 07:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2013-09-10 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 80.6,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(10,'34-SC-35', 201234921, TO_DATE('08/agosto/2014','DD/MON/YY'), 04, TO_TIMESTAMP('2018-12-21 21:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2018-12-21 23:00:00', 'yyyy-mm-dd hh24:mi:ss'), 100,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(11,'11-FG-56', 283741009, TO_DATE('23/abril/2011','DD/MON/YY'), 02, TO_TIMESTAMP('2016-06-16 03:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2016-06-16 03:45:00', 'yyyy-mm-dd hh24:mi:ss'), 27.8,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(12,'12-OB-99', 211234636, TO_DATE('21/maio/2017','DD/MON/YY'), 01, TO_TIMESTAMP('2017-08-31 20:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2017-08-31 21:00:00', 'yyyy-mm-dd hh24:mi:ss'), 32,'N' );
INSERT INTO Pedidos_Viagens(cod_pedido,matricula, nr_idcivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    Values(13,'45-PD-98', 211234636, TO_DATE('25/janeiro/2018','DD/MON/YY'), 03, TO_TIMESTAMP('2018-09-12 10:00:00', 'yyyy-mm-dd hh24:mi:ss'), TO_TIMESTAMP('2018-09-12 10:31:00', 'yyyy-mm-dd hh24:mi:ss'), 34.8,'N' );
    
   --## tabela Viagens ##
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(1,1,10,60);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(2,2,10,100);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(3,3,20,80);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(4,4,40,120);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(5,5,50,120);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(6,6,10,60);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(7,7,20,240);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(8,8,15,180);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(9,9,30,160);
 INSERT into Viagens(cod_viagem, cod_pedido,atraso_passageiro_minutos,duracao_minutos)VALUES(10,10,10,60);
 



 
 
 
 -- ## tabela Pontos_Turisticos ##
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(1,'Torre dos Cl�rigos', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(2,'Torre de Belem', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(3,'Palacio Nacional da Pena', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(4,'Mosteiro dos Jer�nimos', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(5,'Castelo de S.Jorge', 'M');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(6,'Museu Nacional Arte Antiga', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(7,'Museu Nacional das Coches', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(8,'Museu Nacional do Azulejo', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(9,'Museu Calouste Gulbenkian', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(10,'Museu Arte Arquitetura e Tecnologia', 'MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(11,'Parque Nacional Peneda-Ger�s', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(12,'Parque Natural da Arr�bida', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(13,'Parque Natural de Sintra-Cascais', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(14,'Parque Natural do Alv�o', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(15,'Parque Natural da Serra da Estrela', 'PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(16,'Miradouro da Vit�ria', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(17,'Miradouro da Serra do Pilar', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(18,'Miradouro Santa Catarina', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(19,'Miradouro das Fontainhas', 'MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico, nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES(20,'Miradouro do Estu�rio do Douro', 'MI');
 
 
 
 
--## tabela Itenerarios_Viagens##
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(1,1,13);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(2,6,22);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(3,11,19);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(4,16,07);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(5,5,09);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(6,2,11);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(7,17,12);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(8,8,16);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(9,13,18);
INSERT INTO itenerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)
    VALUES(10,4,23);
    




--## tabela Custos_Servicos##
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(01, TO_DATE('31/maio/2018','DD/MON/YY'), TO_DATE('31/maio/2018','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(03, TO_DATE('16/novembro/2017','DD/MON/YY'), TO_DATE('16/novembro/2017','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(03, TO_DATE('30/abril/2019','DD/MON/YY'), TO_DATE('01/maio/2019','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(04, TO_DATE('03/julho/2017','DD/MON/YY'), TO_DATE('03/julho/2017','DD/MON/YY'), 10.00, 4.50, 3.05, 0.23, 10, 1.35, 8.35);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(02, TO_DATE('12/janeiro/2016','DD/MON/YY'), TO_DATE('13/janeiro/2016','DD/MON/YY'), 1.50, 0.20, 0.70, 0.23, 5, 0.10, 1.80);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(03, TO_DATE('24/agosto/2019','DD/MON/YY'), TO_DATE('24/agosto/2019','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(03, TO_DATE('01/maio/2019','DD/MON/YY'), TO_DATE('01/maio/2019','DD/MON/YY'),2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(01, TO_DATE('10/setembro/2013','DD/MON/YY'), TO_DATE('10/setembro/2013','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(01, TO_DATE('21/dezembro/2018','DD/MON/YY'), TO_DATE('21/dezembro/2018','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(04, TO_DATE('16/junho/2015','DD/MON/YY'), TO_DATE('16/junho/2015','DD/MON/YY'), 10.00, 4.50, 3.05, 0.23, 10, 1.35, 8.350);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(02, TO_DATE('20/mar�o/2014','DD/MON/YY'), TO_DATE('20/mar�o/2014','DD/MON/YY'), 1.50, 0.20, 0.70, 0.23, 5, 0.10, 1.80);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(01, TO_DATE('31/agosto/2017','DD/MON/YY'), TO_DATE('31/agosto/2017','DD/MON/YY'), 3.00, 1.00, 0.90, 0.23, 5, 0.10, 2.20);
INSERT INTO Custos_Servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido)
    Values(03, TO_DATE('12/setembro/2018','DD/MON/YY'), TO_DATE('12/setembro/2018','DD/MON/YY'), 2.20, 0.50, 1.00, 0.23, 5, 0.20, 2.20);
    
